
global.$ = global.jQuery = require('./painel/js/jquery/dist/jquery.min.js');

require('./painel/js/select2/dist/js/select2.full.min.js');
require('./painel/js/ckeditor/ckeditor.js');
require('./painel/js/bootstrap-validator/dist/validator.min.js');
require('./painel/js/dropzone/dist/dropzone.js');
require('./painel/js/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js');
global.Tether  = require('./painel/js/tether/dist/js/tether.min.js');
require('./painel/js/bootstrap/js/dist/util.js');
require('./painel/js/bootstrap/js/dist/alert.js');
require('./painel/js/bootstrap/js/dist/button.js');
require('./painel/js/bootstrap/js/dist/carousel.js');
require('./painel/js/bootstrap/js/dist/collapse.js');
require('./painel/js/bootstrap/js/dist/dropdown.js');
require('./painel/js/bootstrap/js/dist/modal.js');
require('./painel/js/bootstrap/js/dist/tab.js');
global.Tooltip  = require('./painel/js/bootstrap/js/dist/tooltip.js');
require('./painel/js/bootstrap/js/dist/popover.js');
require('./painel/js/dragula.js/dist/dragula.min.js');
require('./painel/js/main.js');
<div class="content-box">
    <form id="formValidate" action="/painel/planos/contratar" method="POST" novalidate="true">
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
            <div class="element-wrapper">
                <div class="element-box">
                    <input name="_token" value="Xj3RdmLrZWC21BftIi9uCp9nVGUCpq1MMD6Vauc7" type="hidden">
                    <input type="hidden" name="plan_code" value="{{ $plano->plan_code }}">
                    <h6 class="element-header">Alteração de Assinatura </h6>
                    <div class="form-group">
                        <label for="">Nome do Titular</label>
                        <input class="form-control" name="holder_name" placeholder="Nome do titular do cartão" readonly required="required" value="{{ $subscription->holder_name }}" type="text">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Data de Nascimento</label>
                                <input class="form-control" name="dt_nascimento" id="dt_nascimento" readonly data-mask="00/00/0000" placeholder="Data de nascimento" required="required" value="{{ $subscription->user->dt_nascimento }}" maxlength="10" autocomplete="off" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">CPF</label>
                                <input class="form-control" name="holder_cpf" placeholder="CPF" id="cpf" readonly data-mask="000.000.000-00" required="required" value="{{ $subscription->user->cpf }}" maxlength="14" autocomplete="off" type="text">
                            </div>
                        </div>
                    </div>
                    <h6 class="element-header">Cartão de Crédito</h6>
                    <div class="value w-100  credit-card">
                        <div class="fancy-selector-w mt-25">
                            <div class="fancy-selector-current">
                                <div class="fs-img">
                                    <img alt="" src="/images/card1.png"></div>
                                <div class="fs-main-info">
                                    <div class="fs-name">{{ $subscription->brand }}</div>
                                </div>
                                <div class="fs-extra-info">
                                    <strong>{{ $subscription->last_card }}</strong>
                                    <span>Final</span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
            <div class="element-wrapper">
                <div class="element-box">
                    <h6 class="element-header">Resumo do Pedido</h6>
                    <div class="summary-order">
                        <span class="summary-order-label">Pedido</span>
                        <p class="summary-order-description">
                            <span class="summary-order-text">Plano {{ $plano->name }} <small style="font-weight: bold;color: #c9c9c9 !important">{{ strtoupper($plano->plan_code)  }}</small></span>
                        </p>
                        <span class="summary-order-label">Forma de pagamento</span>
                        <p class="summary-order-description">
                            <span class="summary-order-text">Cartão de crédito</span>
                        </p>
                    </div>
                    <div class="summary-order-bottom">
                        <span class="summary-order-total">Total a pagar:</span>
                        <span class="summary-order-value">R$ {{ str_replace('.', ',', $plano->value) }}0* mensal</span>
                    </div>
                    <button type="submit" onclick="disable(this)" class="summary-order-button">
                        Finalizar Alteração
                        <span class="summary-order__button__icon summary-order__button__icon--padlock"></span>
                    </button>
                    <div class="summary-order__operation">
                        <span class="summary-order-installments" style="margin-top:5px">
                            * Seu pagamento será processado pela <a href="">moip</a>, mas dúvidas com relação ao pagamento e suporte devem ser direcionadas ao
                            nosso <a href="">suporte técnico</a>
                        </span>
                        <span class="summary-order-installments" style="margin-top:5px">
                           * Ao contratar o plano de assinaturas da Octuz, você estará aceitando os <a href="">termos e politicas de uso de assinaturas</a>
                        </span>
                    </div>
                    <div class="summary-order__operation">
                        <img src="{{ asset('images/moip30px.png') }}" class="pull-right" alt="powered by moip">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
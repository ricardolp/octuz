<div class="content-box">
    <form id="formValidate" action="/painel/planos/contratar" method="POST" novalidate="true">
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
            <div class="element-wrapper">
                <div class="element-box">
                    <input name="_token" value="Xj3RdmLrZWC21BftIi9uCp9nVGUCpq1MMD6Vauc7" type="hidden">
                    <input type="hidden" name="plan_code" value="{{ $plano->plan_code }}">
                    <h6 class="element-header">Dados </h6>
                    @if(Session::has('error'))
                        <div class="alert alert-danger" role="alert">{{ Session::get('error') ?: 'Ocorreu um erro, verifique se as informações estão corretas e tente novamente' }}</div>
                    @endif
                    <div class="form-group">
                        <label for="">Nome do Titular</label>
                        <input class="form-control" name="holder_name" placeholder="Nome do titular do cartão" required="required" value="" type="text">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Data de Nascimento</label>
                                <input class="form-control" name="dt_nascimento" id="dt_nascimento" data-mask="00/00/0000" placeholder="Data de nascimento" required="required" value="" maxlength="10" autocomplete="off" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">CPF</label>
                                <input class="form-control" name="holder_cpf" placeholder="CPF" id="cpf" data-mask="000.000.000-00" required="required" value="" maxlength="14" autocomplete="off" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Telefone</label>
                                <input class="form-control" name="fone" data-mask="(99) 9 9999-9999" placeholder="Número de telefone" required="required" value="" autocomplete="off" type="text">
                            </div>
                        </div>
                    </div>
                    <h6 class="element-header">Dados do Cartão de Crédito</h6>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="card-label" for="card">Cartão de Crédito</label>
                            <div name="card-container" class="form-group" style="padding-top: 0px !important;">
                                <input type="text" name="credit_card" id="card" placeholder="0000 0000 0000 0000" class="form-control"  autocomplete="off" maxlength="16" />
                                <div id="logo"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Validade</label>
                                <input class="form-control" name="validade" id="" data-mask="00/00" placeholder="00/00" required="required" autocomplete="off" type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
            <div class="element-wrapper">
                <div class="element-box">
                    <h6 class="element-header">Resumo do Pedido</h6>
                    <div class="summary-order">
                        <span class="summary-order-label">Pedido</span>
                        <p class="summary-order-description">
                            <span class="summary-order-text">Plano {{ $plano->name }} <small style="font-weight: bold;color: #c9c9c9 !important">{{ strtoupper($plano->plan_code)  }}</small></span>
                        </p>
                        <span class="summary-order-label">Forma de pagamento</span>
                        <p class="summary-order-description">
                            <span class="summary-order-text">Cartão de crédito</span>
                        </p>
                    </div>
                    <div class="summary-order-bottom">
                        <span class="summary-order-total">Total a pagar:</span>
                        <span class="summary-order-value">R$ {{ str_replace('.', ',', $plano->value) }}0* mensal</span>
                    </div>
                    <button type="submit" onclick="disable(this)" class="summary-order-button">
                        Finalizar compra
                        <span class="summary-order__button__icon summary-order__button__icon--padlock"></span>
                    </button>
                    <div class="summary-order__operation">
                        <span class="summary-order-installments" style="margin-top:5px">
                            * Seu pagamento será processado pela <a href="">moip</a>, mas dúvidas com relação ao pagamento e suporte devem ser direcionadas ao
                            nosso <a href="">suporte técnico</a>
                        </span>
                        <span class="summary-order-installments" style="margin-top:5px">
                           * Ao contratar o plano de assinaturas da Octuz, você estará aceitando os <a href="">termos e politicas de uso de assinaturas</a>
                        </span>
                    </div>
                    <div class="summary-order__operation">
                        <img src="{{ asset('images/moip30px.png') }}" class="pull-right" alt="powered by moip">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@extends('painel.layout')

@section('content')
    <div class="section-heading centered">
        <h1>Tabela de Planos</h1>
        <p>Caso você já tenha pago um valor cobrado indevidamente, a Octuz irá recuperar o valor pago para você.</p>
    </div>

    <div class="pricing-plans row no-gutters">
        <div class="pricing-plan col-sm-4">
            <div class="plan-head">
                <div class="plan-image">
                    <img alt="" src="/images/plan1.png">
                </div>
                <div class="plan-name">Pontual</div>
            </div>
            <div class="plan-body">
                <div class="plan-price-w">
                    <div class="price-value">R$ 12,90</div>
                    <div class="price-label">Por Caso Resolvido</div>
                </div>
                <div class="plan-btn-w">
                    <a href="/painel/assinatura/contratar/pontual" data-id="plano03" class="btn btn-primary btn-rounded contract {!! Auth::user()->plan_id === 3 || is_null(Auth::user()->plan_id) ? 'disabled' : ''!!}"  href="#">
                        {{ Auth::user()->plan_id === 3  || is_null(Auth::user()->plan_id) ? 'Este é seu plano atual' : 'Contratar' }}
                    </a>
                </div>
            </div>
            <div class="plan-description">
                <h6>Descrição</h6>
                <p>Esse plano não oferece nenhum vínculo coma a Octuz você só paga por caso resolvido e após a resolução do caso,
                    ele lhe dá direito a passar para Octuz problemas relacionados à:</p>
                <ul>
                    <li>Cobranças</li>
                    <li>Reembolso</li>
                    <li>Créditos </li>
                </ul>
            </div>
        </div>
        <div class="pricing-plan col-sm-4 highlight">
            <div class="plan-head">
                <div class="plan-image"><img alt="" src="/images/plan2.png"></div>
                <div class="plan-name">Mensal</div>
            </div>
            <div class="plan-body">
                <div class="plan-price-w">
                    <div class="price-value">R$ 9,90</div>
                    <div class="price-label">Por Mês</div>
                </div>
                <div class="plan-btn-w">
                    <a href="/painel/assinatura/contratar/mensal" data-id="plano01" class="btn btn-primary btn-rounded contract {!! Auth::user()->plan_id === 1 ? 'disabled' : ''!!}"   href="#">
                        {{ Auth::user()->plan_id === 1  ? 'Este é seu plano atual' : 'Contratar' }}
                    </a>
                    @if(Auth::user()->plan_id === 1)
                        <br>
                        <br>
                        <a href="/painel/assinatura/cancelar" style="color: #767676;">CANCELAR</a>
                    @endif
                </div>
            </div>
            <div class="plan-description">
                <h6>Descrição</h6>
                <p>Esse plano é renovado mensalmente e necessita do cadastro de um cartão de crédito para ser contratado,
                    ele lhe dá direito a passar para Octuz problemas relacionados à:</p>
                <ul>
                    <li>Cobranças</li>
                    <li>Reembolso</li>
                    <li>Créditos </li>
                    <li>Acordos financeiros</li>
                </ul>
            </div>
        </div>
        <div class="pricing-plan col-sm-4">
            <div class="plan-head">
                <div class="plan-image"><img alt="" src="/images/plan4.png"></div>
                <div class="plan-name">Semestral</div>
            </div>
            <div class="plan-body">
                <div class="plan-price-w">
                    <div class="price-value">R$ 6,90</div>
                    <div class="price-label">Por Mês</div>
                </div>
                <div class="plan-btn-w">
                    <a href="/painel/assinatura/contratar/anual" data-id="plano02" class="btn btn-primary btn-rounded contract {!! Auth::user()->plan_id === 2 ? 'disabled' : ''!!}"   href="#">
                        {{ Auth::user()->plan_id === 2  ? 'Este é seu plano atual' : 'Contratar' }}
                    </a>
                    @if(Auth::user()->plan_id === 2)
                        <br>
                        <br>
                        <a href="/painel/assinatura/cancelar" style="color: #767676;">CANCELAR</a>
                    @endif
                </div>
            </div>
            <div class="plan-description">
                <h6>Descrição</h6>
                <p>Esse plano é renovado anual, sendo cobrado as mensalidades no valor informado acima, ele lhe dá direito a passar para Octuz problemas relacionados à:</p>
                <ul>
                    <li>Cobranças</li>
                    <li>Reembolso</li>
                    <li>Créditos </li>
                    <li>Acordos financeiros</li>
                </ul>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('js/assinatura.js') }}"></script>
@endsection


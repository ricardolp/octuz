@extends('painel.layout')

@section('content')
    @if(Auth::user()->plan_id === 3 || is_null(Auth::user()->plan_id))
        @include('painel.planos.layout.create')
    @else
        @include('painel.planos.layout.change')
    @endif
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-masker/1.2.0/vanilla-masker.min.js"></script>
    <script src="/js/app.bundle.js"></script>
    <script>
        function disable(el) {
            $form = document.getElementById('formValidate').checkValidity()

            if($form){
                $(el).attr('disabled', true)
                $(el).empty()
                $(el).append('<img src="/images/loading.gif" style="max-height: 35px;" alt="Aguarde">')
            } else {

            }
        }
    </script>
@endsection
@extends('painel.layout')

@section('content')
    <div class="content-box">
        <form id="formValidate" action="/painel/assinatura/pagamento" method="POST" novalidate="true">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
                <div class="element-wrapper">
                    <div class="element-box">
                        <h6 class="element-header">Dados do Cartão de Crédito</h6>
                        @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">{{ Session::get('error') ?: 'Ocorreu um erro, verifique se as informações estão corretas e tente novamente' }}</div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Nome do Titular</label>
                                    <input class="form-control" name="holder_name" placeholder="Nome do titular do cartão" required="required" value="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="card-label" for="card">Cartão de Crédito</label>
                                <div name="card-container" class="form-group" style="padding-top: 0px !important;">
                                    <input type="text" name="credit_card" id="card" placeholder="0000 0000 0000 0000" class="form-control"  autocomplete="off" maxlength="16" />
                                    <div id="logo"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Validade</label>
                                    <input class="form-control" name="validade" id="" data-mask="00/00" placeholder="00/00" required="required" autocomplete="off" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" onclick="disable(this)" class="summary-order-button" style="margin-top: 0">
                                    Alterar
                                    <span class="summary-order__button__icon summary-order__button__icon--padlock"></span>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                               <div class="summary-order__operation">
                                    <span class="summary-order-installments" style="margin-top:5px">
                                       * A cobrança será realizada apenas no próximo vencimento da assinatura
                                    </span>
                                   <span class="summary-order-installments" style="margin-top:5px">
                                        * Seu pagamento será processado pela <a href="">moip</a>, mas dúvidas com relação ao pagamento e suporte devem ser direcionadas ao
                                        nosso <a href="">suporte técnico</a>
                                    </span>
                                   <span class="summary-order-installments" style="margin-top:5px">
                                       * Ao contratar o plano de assinaturas da Octuz, você estará aceitando os <a href="">termos e politicas de uso de assinaturas</a>
                                    </span>
                               </div>
                               <div class="summary-order__operation">
                                   <img src="{{ asset('images/moip30px.png') }}" class="pull-right" alt="powered by moip">
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-masker/1.2.0/vanilla-masker.min.js"></script>
    <script src="/js/app.bundle.js"></script>
    <script>
        function disable(el) {
            $form = document.getElementById('formValidate').checkValidity()

            if($form){
                $(el).attr('disabled', true)
                $(el).empty()
                $(el).append('<img src="/images/loading.gif" style="max-height: 35px;" alt="Aguarde">')
            } else {

            }
        }
    </script>
@endsection
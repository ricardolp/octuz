@extends('painel.layout')

@section('content')
    <div class="content-box">
        <form id="formValidate" action="/painel/assinatura/cancelar" method="POST" novalidate="true">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="float:left">
                <div class="element-wrapper">
                    <div class="element-box">
                        <h6 class="element-header">Cancelar Assinatura</h6>
                        <div class="row">
                           <div class="col-md-12">
                               <p>
                                   Seu feedback é muito importante para nós sabermos o que devemos melhorar em nossos serviços,
                                   por favor preencha e os campos abaixo e confirme o cancelamento.
                               </p>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Motivo do cancelamento</label>
                                    <select name="motivo" id="" class="form-control" required>
                                        <option value="Não gostei do serviço">Não gostei do serviço</option>
                                        <option value="Não vejo utilidade">Não vejo utilidade</option>
                                        <option value="Não resolveram nenhum caso">Não resolveram nenhum caso</option>
                                        <option value="O valor está caro">O valor está caro</option>
                                        <option value="Encontrei uma opção melhor">Encontrei uma opção melhor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Comentários</label>
                                    <textarea name="comment" id=""class="form-control"  cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" onclick="disable(this)" class="summary-order-button" style="margin-top: 0">
                                    Confirmar cancelamento
                                    <span class="summary-order__button__icon summary-order__button__icon--padlock"></span>
                                </button>
                            </div>
                        </div>
                        <div class="summary-order__operation">
                        <span class="summary-order-installments" style="margin-top:5px">
                            * Esta ação é irreversivel, para voltar a utilizar a Octuz em um plano de pagamenro, será necessário a contratação
                            de uma nova assinatura.
                        </span>
                            <span class="summary-order-installments" style="margin-top:5px">
                           * Um email de confirmação será enviado após o cancelamento
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-masker/1.2.0/vanilla-masker.min.js"></script>
    <script src="/js/app.bundle.js"></script>
    <script>
        function disable(el) {
            $form = document.getElementById('formValidate').checkValidity()

            if($form){
                $(el).attr('disabled', true)
                $(el).empty()
                $(el).append('<img src="/images/loading.gif" style="max-height: 35px;" alt="Aguarde">')
            } else {

            }
        }
    </script>
@endsection
<!DOCTYPE html>
<html>
<head>
    <title>Octuz Painel</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link rel="stylesheet" href="/css/themify.css">
    <link rel="stylesheet" href="/css/painel.css">
</head>
<body data-id="{{ Auth::user()->id }}" >
<div class="all-wrapper menu-side" >
    <div class="layout-w">
        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
            <div class="mm-logo-buttons-w">
                <a class="mm-logo" href="/"><img src="/images/logo_dot.png"><span>Octuz</span></a>
            </div>
            <div class="menu-and-user">
                <div class="logged-user-w">
                    <div class="avatar-w"><img alt="" src="{{ Auth::user()->path_url ?: '/images/user.png' }}"></div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">{{ Auth::user()->name }}</div>
                    </div>
                </div>

                <ul class="main-menu">
                    <li>
                        <a href="/painel">
                            <div class="icon-w">
                                <div class="os-icon os-icon-window-content"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/casos">
                            <div class="icon-w">
                                <div class="os-icon os-icon-hierarchy-structure-2"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/planos">
                            <div class="icon-w">
                                <div class="os-icon os-icon-newspaper"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/minha-conta">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="icon-w">
                                <div class="os-icon os-icon-mail-07"></div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="desktop-menu menu-side-compact-w menu-activated-on-hover color-scheme-dark">
            <div class="logo-w"><a class="logo" href="/"><img src="/images/logo_dot.png"></a></div>
            <div class="menu-and-user">
                <div class="logged-user-w">
                    <div class="logged-user-i">
                        <div class="avatar-w"><img alt="" src="{{ Auth::user()->path_url ?: '/images/user.png' }}"></div>
                        <div class="logged-user-menu">
                            <div class="logged-user-avatar-info">
                                <div class="avatar-w"><img alt="" src="{{ Auth::user()->path_url ?: '/images/user.png' }}"></div>
                                <div class="logged-user-info-w">
                                    <div class="logged-user-name">{{ Auth::user()->name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="main-menu">
                    <li>
                        <a href="/painel/casos">
                            <div class="icon-w">
                                <div class="os-icon os-icon-hierarchy-structure-2"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/minha-conta">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/assinatura">
                            <div class="icon-w">
                                <div class="os-icon os-icon-calendar-time"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="/painel/recibos">
                            <div class="icon-w">
                                <div class="os-icon os-icon-tasks-checked"></div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="content-w">
            <div class="top-menu-secondary color-scheme-dark">
                <div class="top-menu-controls">
                    <div class="element-search hidden-lg-down"><input placeholder="Digite para pesquisar..." type="text"></div>
                    <div class="top-icon top-search hidden-xl-up"><i class="os-icon os-icon-ui-37"></i></div>
                    <div class="messages-notifications os-dropdown-trigger os-dropdown-center">
                        <i class="os-icon os-icon-mail-14"></i>
                        <div class="os-dropdown light message-list">
                            <div class="icon-w"><i class="os-icon os-icon-mail-14"></i></div>
                        </div>
                    </div>

                    <div class="top-icon top-settings os-dropdown-trigger os-dropdown-center">
                        <i class="os-icon os-icon-ui-46"></i>
                        <div class="os-dropdown">
                            <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>
                            <ul>
                                <li><a href="#"><i class="os-icon os-icon-ui-49"></i><span>Minha Conta</span></a></li>
                                <li><a href="#"><i class="os-icon os-icon-ui-44"></i><span>Meus Casos</span></a></li>
                                <li><a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="os-icon os-icon-signs-11"></i><span>Sair</span></a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-i">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="display-type"></div>
</div>

<script src="/js/painel/jquery/dist/jquery.min.js"></script>
<script>
    $(".pipeline-body").append('<img src="/images/loader.gif" style="max-width: 100%;"/>')
</script>

<script src="https://assets.moip.com.br/v2/moip-2.8.0.min.js"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/assets-sandbox.moip.com.br/assinaturas/moip-assinaturas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js" type="text/javascript"></script>
<script src="http://webapplayers.com/inspinia_admin-v2.7.1/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/painel/select2/dist/js/select2.full.min.js"></script>
<script src="/js/painel/ckeditor/ckeditor.js"></script>
<script src="/js/painel/bootstrap-validator/dist/validator.min.js"></script>
<script src="/js/painel/dropzone/dist/dropzone.js"></script>
<script src="/js/painel/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/js/painel/tether/dist/js/tether.min.js"></script>
<script src="/js/painel/bootstrap/js/dist/util.js"></script>
<script src="/js/painel/bootstrap/js/dist/alert.js"></script>
<script src="/js/painel/bootstrap/js/dist/button.js"></script>
<script src="/js/painel/bootstrap/js/dist/carousel.js"></script>
<script src="/js/painel/bootstrap/js/dist/collapse.js"></script>
<script src="/js/painel/bootstrap/js/dist/dropdown.js"></script>
<script src="/js/painel/bootstrap/js/dist/modal.js"></script>
<script src="/js/painel/bootstrap/js/dist/tab.js"></script>
<script src="/js/painel/bootstrap/js/dist/tooltip.js"></script>
<script src="/js/painel/bootstrap/js/dist/popover.js"></script>
<script src="/js/painel/dragula.js/dist/dragula.min.js"></script>
<script src="/js/jquery-bootstrap-modal-steps.js"></script>
<script src="/js/painel/main.js"></script>
<script src="/js/painel/painel.js"></script>
@yield('script')
</body>
</html>


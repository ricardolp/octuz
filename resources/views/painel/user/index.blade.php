@extends('painel.layout')

@section("content")
    <div class="content-box">
        <div class="row">
            <div class="col-lg-12">
                {{--@if(Session::has('error'))
                    <div class="alert alert-danger w-100" role="alert">
                        <strong>Atenção! </strong> Você já possui uma assinatura ativa ou atrasada, você deve cancelar a aassinatura
                        atual antes de contratar uma nova assinatura.
                    </div>
                @endif--}}
                @if(Session::has('success'))
                    @if(Session::get('success') === 1)
                        <div class="alert alert-success w-100" role="alert">
                            <strong>Atualizado! </strong> Seu cadastro foi atualizado com sucesso
                        </div>
                    @elseif(Session::get('success') === 2)
                        <div class="alert alert-success w-100" role="alert">
                            <strong>Atualizado! </strong> Sua assinatura foi alterada, enviamos um e-mail com as informações adicionais.
                        </div>
                    @endif

                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="user-profile compact">
                    <div class="up-head-w" style="text-align: center;vertical-align: middle;padding: 70px;background: #018acd;color:white">
                        <div class="value">
                            <i class="os-icon os-icon-robot-1" style="font-size: 5rem;color: white;text-align: center;"></i>
                            <div class="up-main-info p-5-5-5">
                                <h2 class="up-header">MINHA CONTA</h2>
                            </div>
                        </div>
                        <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF"><path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path></g></svg>
                    </div>
                    <div class="up-controls">
                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <a class="btn btn-sm btn-f pull-right" href="">
                                    <i class="icon-social-facebook"></i>
                                    <span>Facebook</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="up-contents">
                        <div class="m-b">
                            <div class="row m-b">
                                <div class="col-sm-4 b-r b-b">
                                    <div class="el-tablo centered padded-v">
                                        <div class="value" style="font-size: 1.8rem;">{{ $casos }}</div>
                                        <div class="label">CASOS</div>
                                    </div>
                                </div>
                                <div class="col-sm-4 b-b">
                                    <div class="el-tablo centered padded-v">
                                        <div class="value">
                                            <i class="icon-check"></i>
                                        </div>
                                        <div class="value" style="font-size: 1.8rem;">{{ $resolvidos }}</div>
                                        <div class="label">RESOLVIDOS</div>
                                    </div>
                                </div>
                                <div class="col-sm-4 b-b">
                                    <div class="el-tablo centered padded-v">
                                        <div class="value" style="font-size: 1.8rem;">R$ {{ $recuperados }}</div>
                                        <div class="label">RECUPERADOS</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-b">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                            <tr>
                                                <th colspan="3">Minhas Linhas Telêfonicas</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @if(Auth::user()->account->count() === 0)
                                                    <tr>
                                                        <td class="nowrap text-center" >
                                                            Nenhuma conta cadastrada
                                                        </td>
                                                    </tr>
                                                @else
                                                    @foreach(Auth::user()->account as $account)
                                                        <tr>
                                                            <td class="nowrap">
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" title="{!! $account->empresa->name !!}" style="background-image: url({!! $account->empresa->path_logo !!})"></div>
                                                                </div>
                                                            </td>
                                                            <td class="nowrap">{{ $account->login ?: '' }}</td>
                                                            <td><span>{{ $account->numero_conta }}</span></td>
                                                            <td>Icone</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="element-wrapper">
                    <div class="element-box">
                        <form id="formValidate" action="/painel/minha-conta" method="POST" novalidate="true">
                            {{ csrf_field() }}
                            <div class="element-info">
                                <div class="element-info-with-icon">
                                    <div class="element-info-icon">
                                        <div class="ti-user"></div>
                                    </div>
                                    <div class="element-info-text">
                                        <h5 class="element-inner-header">Informações Pessoais</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""> Nome</label>
                                <input class="form-control" name="name" placeholder="Nome" required="required" readonly="" value="{{ Auth::user()->name }}" type="text">
                            </div>
                            <div class="form-group">
                                <label for=""> Email</label>
                                <input class="form-control" name="email" placeholder="E-mail" required="required" readonly="" value="{{ Auth::user()->email }}" type="email">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Telefone</label>
                                        <input class="form-control" name="fone" id="fone" placeholder="Telefone" data-mask="(99) 9 9999-9999" readonly="" required="required" value="{{ Auth::user()->fone }}" maxlength="16" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> CPF</label>
                                        <input class="form-control" name="cpf" placeholder="CPF" id="cpf" readonly="" data-mask="000.000.000-00" required="required" value="{{ Auth::user()->cpf }}" maxlength="14" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Data de Nascimento</label>
                                        <input class="form-control" name="dt_nascimento" id="dt_nascimento" data-mask="00/00/0000" placeholder="Data de nascimento" readonly="" required="required" value="{{ Auth::user()->dt_nascimento }}" maxlength="10" type="text">
                                    </div>
                                </div>
                                {{--@if(is_null(Auth::user()->facebook_id))
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for=""> Senha</label>
                                            <input class="form-control" name="password" placeholder="Senha" value="*******" readonly="" required="required" type="password">
                                        </div>
                                    </div>
                                @endif--}}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> CEP</label>
                                        <input class="form-control" name="zipcode" id="zipcode" data-mask="99999-999" placeholder="CEP" required="required" readonly="" value="{{ Auth::user()->zipcode }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for=""> Endereço</label>
                                        <input class="form-control" name="address" id="address" placeholder="Endereço" required="required" readonly="" value="{{ Auth::user()->address }}" type="text">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for=""> Número</label>
                                        <input class="form-control" name="number_residence" id="number_residence" placeholder="Endereço" required="required" readonly="" value="{{ Auth::user()->number_residence }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Complemento</label>
                                        <input class="form-control" name="complement" id="complement" placeholder="Complemento" readonly="" value="{{ Auth::user()->complement }}" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Bairro</label>
                                        <input class="form-control" name="district" id="district" placeholder="Bairro" required="required" readonly="" value="{{ Auth::user()->district }}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Estado</label>
                                        <input type="hidden" id="estado_selected">
                                        <select name="state" class="form-control estado readonly" id="state" readonly=""></select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> Cidade</label>
                                        <input type="hidden" id="cidade_selected" class="">
                                        <select name="city" class="form-control cidade readonly" id="city" readonly=""></select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-w">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-default btn-unlock" type="button">Editar</button>
                                        <button class="btn btn-primary btn-submit-f" disabled="" type="submit">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content-panel col-md-4">
                <div class="content-panel-close"><i class="os-icon os-icon-close"></i></div>
                <div class="element-wrapper">
                    <h6 class="element-header">Informações da Assinatura</h6>

                    <div class="alert alert-info borderless text-center">
                        <h5 class="alert-heading">Plano {{ (Auth::user()->plano) ? Auth::user()->plano->name : 'Pontual' }}</h5>
                        @if(Auth::user()->plano)
                            @if(Auth::user()->plano->name === 'Pontual')
                                <p>Seu plano é pontual é cobrado o valor de <b>R$ 12,90</b> por caso resolvido.
                                    As cobranças são enviadas por e-mail após a finalização de seu caso.</p>
                                <button class="mr-2 mb-2 btn btn-primary btn-rounded change-plan" type="button">ALTERAR</button>
                            @else
                                <p>Seu plano será renovado automaticamente no dia <b>{{ $subscription->expired_at->format('d/m/Y') }}</b> e serão cobrados <b>R$ {{ str_replace('.', ',', Auth::user()->plano->value) }}0</b></p>
                                <button class="mr-2 mb-2 btn btn-primary btn-rounded change-plan" type="button">ALTERAR OU CANCELAR</button>
                            @endif
                        @else
                            <p>Seu plano é pontual é cobrado o valor de <b>R$ 12,90</b> por caso resolvido.
                                As cobranças são enviadas por e-mail após a finalização de seu caso.</p>
                            <button class="mr-2 mb-2 btn btn-primary btn-rounded change-plan" type="button">ALTERAR</button>
                        @endif
                    </div>


                    @if(Auth::user()->plano)
                        @if(Auth::user()->plano->name !== 'Pontual')
                            <div class="element-content">
                                <div class="element-box el-tablo">
                                    <div class="label">
                                        Forma de Pagamento
                                        <a href="/painel/assinatura/pagamento" class="mr-2 mb-2 btn btn-primary btn-rounded pull-right" type="button">ALTERAR</a>
                                    </div>
                                    <div class="value w-100  credit-card">
                                        <div class="fancy-selector-w mt-25">
                                            <div class="fancy-selector-current">
                                                <div class="fs-img">
                                                    <img alt="" src="/images/card1.png"></div>
                                                <div class="fs-main-info">
                                                    <div class="fs-name">{{ $subscription->brand }}</div>
                                                </div>
                                                <div class="fs-extra-info">
                                                    <strong>{{ $subscription->last_card }}</strong>
                                                    <span>Final</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endif
                    @endif

               {{-- <div class="element-wrapper">
                    <h6 class="element-header">Informações de pagamento & plano</h6>
                        <div class="element-box el-tablo">
                            <div class="label">Plano de pagamento</div>
                            <div class="value">{{ (Auth::user()->plano) ? Auth::user()->plano->name : 'Pontual' }}</div>
                            <a href="javascript:void(0)" class="hidden-xs hidden-sm change-plan">
                                <div class="trending trending-up right">alterar</div>
                                <div class="trending trending-up right">cancelar</div>
                            </a>
                            <a href="javascript:void(0)" class="hidden-lg hidden-md icon-change-plan change-plan"><i class="os-icon os-icon-edit-1"></i></a>
                        </div>
                    </div>


                </div>
            </div>--}}
        </div>
    @include('painel.user.modal-plan')

    <input type="hidden" id="public_key"
        value="
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoBttaXwRoI1Fbcond5mS
        7QOb7X2lykY5hvvDeLJelvFhpeLnS4YDwkrnziM3W00UNH1yiSDU+3JhfHu5G387
        O6uN9rIHXvL+TRzkVfa5iIjG+ap2N0/toPzy5ekpgxBicjtyPHEgoU6dRzdszEF4
        ItimGk5ACx/lMOvctncS5j3uWBaTPwyn0hshmtDwClf6dEZgQvm/dNaIkxHKV+9j
        Mn3ZfK/liT8A3xwaVvRzzuxf09xJTXrAd9v5VQbeWGxwFcW05oJulSFjmJA9Hcmb
        DYHJT+sG2mlZDEruCGAzCVubJwGY1aRlcs9AQc1jIm/l8JwH7le2kpk3QoX+gz0w
        WwIDAQAB"/>

@endsection

@section('script')
    <script type="text/javascript">

        $(".change-plan").on('click', function(){
            if(checkData() === true){
                return location.href='/painel/assinatura';
            } else {
                swal(
                    'Oops...',
                    'Antes de alterar seu plano, por favor atualize seus dados cadastrais!',
                    'warning'
                )
            }
        });

        $("#billing_zipcode").on('change blur', function(){
            var cep = $(this).val()
            if(cep.length >= 9){
                $.get('https://viacep.com.br/ws/'+cep+'/json/', function(data){
                    $("#billing_address").val(data.logradouro)
                    $("#billing_district").val(data.bairro)
                    $("#billing_city").val(data.localidade)
                    $("#billing_state").val(data.uf)
                })
            }
        })

        $("#zipcode").on('change blur', function(){
            var cep = $(this).val()
            if(cep.length >= 9){
                $.get('https://viacep.com.br/ws/'+cep+'/json/', function(data){
                    $("#address").val(data.logradouro).change()
                    $("#district").val(data.bairro).change()
                    $("#cidade_selected").val(data.localidade).change()
                    $("#estado_selected").val(data.uf).change()
                    $(".estado").val(data.uf).change()
                })
            }
        })

/*        $(".credit-card").on('click', function(){
            swal({
                title: 'Remover Cartão?',
                text: "remover o cartão de crédito pode resultar na falha durante o pagamento!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, Remover!'
            }).then(function (result) {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            })
        })*/

        function checkData()
        {
            var fone = $("#fone").val()
            var cpf = $("#cpf").val()
            var dt_nascimento = $("#dt_nascimento").val()
            var address = $("#address").val()
            var zipcode = $("#zipcode").val()
            var r = true;
            if(fone === null || fone === undefined || fone === ""){
                r=false;
            }

            if(cpf === null || cpf === undefined || cpf === ""){
                r=false;
            }

            if(dt_nascimento === null || dt_nascimento === undefined || dt_nascimento === ""){
                r=false;
            }

            if(address === null || address === undefined || address === ""){
                r=false;
            }

            if(zipcode === null || zipcode === undefined || zipcode === ""){
                r=false;
            }

            return r;
        }

        function displayModal()
        {
            if(checkData() === true){
                $("#modalAddPayment").modal('toggle')
            } else {
                swal(
                    'Atenção!',
                    'Complete seus dados cadastrais antes de adicionar um método de pagamento.',
                    'warning'
                )
            }
        }

        $(document).ready(function() {
            $("#pay_cc").click(function() {
                var cc = new Moip.CreditCard({
                    number  : $("#credit_card").val(),
                    cvc     : $("#billing_cc_cvc").val(),
                    expMonth: $("#expiration_month").val(),
                    expYear : $("#expiration_year").val(),
                    pubKey  : $("#public_key").val()
                });
                if( cc.isValid()){
                    var user = $("body").data("id");
                    $.post('/api/user/'+user+'/card', {
                        brand: Moip.Validator.cardType($("#credit_card").val()).brand,
                        nome_titular: $("#holder_name").val(),
                        cpf_titular: $("#cpf_titular").val(),
                        number: $("#credit_card").val(),
                        expMonth: $("#expiration_month").val(),
                        cvc: $("#billing_cc_cvc").val(),
                        expYear: $("#expiration_year").val(),
                        address: $("#rua").val(),
                        dt_nascimento: $("#dt_nascimento").val(),
                        district: $("#bairro").val(),
                        city: $("#cidade").val(),
                        state: $("#estado").val(),
                        zipcode: $("#cep").val(),
                        number_complement: $("#numero").val()
                    }, function() {

                    }).done(function(){
                        alert('Cartão adicionado com sucesso')
                        location.reload()
                    })
                } else {
                    $("#hash").val('');
                    alert('Invalid credit card. Verify parameters: number, cvc, expiration Month, expiration Year');
                }
            });
        });
    </script>
@endsection
<div aria-labelledby="exampleModalLabel" class="modal fade" id="modalAddPayment" role="dialog" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar método de pagamento</h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" id="fullname" value="{{ Auth::user()->name }}">
                    <input type="hidden" id="email" value="{{ Auth::user()->email }}">
                    <input type="hidden" id="cpf" value="{{ Auth::user()->cpf }}">
                    <input type="hidden" id="rua" value="{{ Auth::user()->address }}">
                    <input type="hidden" id="phone" value="{{ Auth::user()->fone }}">
                    <input type="hidden" id="numero" value="{{ Auth::user()->number_residence }}">
                    <input type="hidden" id="complemento" value="{{ Auth::user()->complement ?: 'casa' }}">
                    <input type="hidden" id="bairro" value="{{ Auth::user()->district }}">
                    <input type="hidden" id="cep" value="{{ Auth::user()->zipcode }}">
                    <input type="hidden" id="cidade" value="{{ Auth::user()->city }}">
                    <input type="hidden" id="estado" value="{{ Auth::user()->state }}">
                    <div class="form-group">
                        <label for="">Nome</label>
                        <input class="form-control" id="holder_name" autocomplete="false" placeholder="Nome do titular" type="text">
                    </div>
                    <div class="form-group col-md-6 p-0 pull-left pr-05">
                        <label for="">CPF</label>
                        <input class="form-control" name="holder_cpf" id="cpf_titular" data-mask="999.999.999-99" autocomplete="false" placeholder="CPF do títular" type="text">
                    </div>
                    <div class="form-group col-md-6 p-0 pull-left">
                        <label for="">Data de Nasicmento</label>
                        <input class="form-control"  id="dt_nascimento" data-mask="99/99/9999" autocomplete="false" placeholder="Dt de Nascimento" type="text">
                    </div>
                    <div class="form-group">
                        <label for="">Número do Cartão</label>
                        <input class="form-control" id="credit_card" placeholder="0000 0000 0000 0000" autocomplete="false" data-mask="9999 9999 9999 9999" type="text">
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group col-sm-6 no-padding pull-left" style="float:left">
                                <label for="">Validade</label>
                                <input class="form-control" id="expiration_month" placeholder="Mês ex: 12" autocomplete="false" data-mask="99" maxlength="2" type="text">
                            </div>
                            <div class="form-group col-sm-6 no-padding pull-left" style="float:left">
                                <label for="">&nbsp;</label>
                                <input class="form-control" id="expiration_year" placeholder="Ano ex: 2020" autocomplete="false" data-mask="9999" maxlength="4" type="text">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">CVV</label>
                                <input class="form-control" id="billing_cc_cvc" placeholder="CVV" type="number" autocomplete="false" maxlength="4">
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-12 p-0">
                    <img src="{{ asset('images/moip30px.png') }}" class="pull-left" alt="powered by moip">
                    <button class="btn btn-primary pull-right" type="button" id="pay_cc"> Adicionar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('painel.layout')

@section('content')
    <div class="content-box">
        <div class="row">
            <div class="col-sm-12">
                <div class="element-wrapper">
                    <h6 class="element-header">Olá {{ Auth::user()->name }}, seja bem vindo!</h6>
                    <div class="element-content">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="element-box el-tablo">
                                    <div class="label">Total de casos</div>
                                    <div class="value">{{ $casos }}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="element-box el-tablo">
                                    <div class="label">Valor já recuperado</div>
                                    <div class="value">R$ {{ $recuperados }}</div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="element-box el-tablo">
                                    <div class="label">Casos em aberto</div>
                                    <div class="value">{{ $total }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


       {{-- <div class="element-wrapper">
            <div class="element-box">
                <h6 class="element-header">Registro de Atividades</h6>
                <div class="timed-activities compact">
                    <div class="timed-activity">
                        <div class="ta-date"><span>21st Jan, 2017</span></div>
                        <div class="ta-record-w">
                            <div class="ta-record">
                                <div class="ta-timestamp"><strong>11:55</strong> am</div>
                                <div class="ta-activity">Created a post called <a href="#">Register new symbol</a> in Rogue</div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp"><strong>2:34</strong> pm</div>
                                <div class="ta-activity">Commented on story <a href="#">How to be a leader</a> in <a href="#">Financial</a> category</div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp"><strong>7:12</strong> pm</div>
                                <div class="ta-activity">Added <a href="#">John Silver</a> as a friend</div>
                            </div>
                        </div>
                    </div>
                    <div class="timed-activity">
                        <div class="ta-date"><span>3rd Feb, 2017</span></div>
                        <div class="ta-record-w">
                            <div class="ta-record">
                                <div class="ta-timestamp"><strong>9:32</strong> pm</div>
                                <div class="ta-activity">Added <a href="#">John Silver</a> as a friend</div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp"><strong>5:14</strong> pm</div>
                                <div class="ta-activity">Commented on story <a href="#">How to be a leader</a> in <a href="#">Financial</a> category</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
@endsection
@extends('painel.layout')

@section('content')
    <div class="content-box">
    <div class="element-wrapper">
        <div class="element-box">
            <form>
                <div class="steps-w">
                    <div class="step-triggers"><a class="step-trigger active" href="#stepContent1">First Step</a><a class="step-trigger" href="#stepContent2">Second Step</a><a class="step-trigger" href="#stepContent3">Third Step</a></div>
                    <div class="step-contents">
                        <div class="step-content active" id="stepContent1">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group"><label for=""> First Name</label><input class="form-control" placeholder="First Name" type="text"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group"><label for="">Last Name</label><input class="form-control" placeholder="Last Name" type="text"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group"><label for=""> Date of Birth</label><input class="form-control" placeholder="First Name" type="text"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group"><label for="">Country of Origin</label><input class="form-control" placeholder="Last Name" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""> States with Offices</label>
                                <select class="form-control select2" multiple="true">
                                    <option selected="true">New York</option>
                                    <option selected="true">California</option>
                                    <option>Boston</option>
                                    <option>Texas</option>
                                    <option>Colorado</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for=""> Regular select</label>
                                <select class="form-control">
                                    <option>Select State</option>
                                    <option>New York</option>
                                    <option>California</option>
                                    <option>Boston</option>
                                    <option>Texas</option>
                                    <option>Colorado</option>
                                </select>
                            </div>
                            <div class="form-group"><label> Example textarea</label><textarea class="form-control" rows="3"></textarea></div>
                            <div class="form-buttons-w text-right"><a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a></div>
                        </div>
                        <div class="step-content" id="stepContent2">
                            <div class="form-group"><label for=""> Email address</label><input class="form-control" placeholder="Enter email" type="email"></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group"><label for=""> Password</label><input class="form-control" placeholder="Password" type="password"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group"><label for="">Confirm Password</label><input class="form-control" placeholder="Password" type="password"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""> Regular select</label>
                                <select class="form-control">
                                    <option>Select State</option>
                                    <option>New York</option>
                                    <option>California</option>
                                    <option>Boston</option>
                                    <option>Texas</option>
                                    <option>Colorado</option>
                                </select>
                            </div>
                            <div class="form-buttons-w text-right"><a class="btn btn-primary step-trigger-btn" href="#stepContent3"> Continue</a></div>
                        </div>
                        <div class="step-content" id="stepContent3">
                            <div class="form-group">
                                <label for=""> Regular select</label>
                                <select class="form-control">
                                    <option>Select State</option>
                                    <option>New York</option>
                                    <option>California</option>
                                    <option>Boston</option>
                                    <option>Texas</option>
                                    <option>Colorado</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group"><label for=""> Date of Birth</label><input class="form-control" placeholder="First Name" type="text"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group"><label for="">Country of Origin</label><input class="form-control" placeholder="Last Name" type="text"></div>
                                </div>
                            </div>
                            <div class="form-buttons-w text-right"><button class="btn btn-primary">Submit Form</button></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
@endsection
@extends('painel.layout')

@section('content')
    @include('painel.components.modal')

    <ul class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/painel">Ínicio</a>
    </li>
    <li class="breadcrumb-item">
        <span>Meus Casos</span>
    </li>
</ul>

<div class="content-box">
    <div class="pipelines-w">
        <div class="row">
            <div class="col-lg-3 col-xxl-3">
                <div class="element-wrapper">
                    <div class="element-box-tp">
                        <div class="el-buttons-list full-width">
                            <a class="btn btn-white btn-sm" href="#" onclick="$('#modalTelefonica').modal('toggle')">
                                <i class="os-icon os-icon-delivery-box-2"></i>
                                <span>Adicionar um novo caso</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="pipeline white lined-primary">
                    <div class="pipeline-header">
                        <h5 class="pipeline-name">Meus Casos</h5>
                        <div class="pipeline-header-numbers">
                            <div class="pipeline-count"></div>
                        </div>
                        <div class="pipeline-settings os-dropdown-trigger">
                            <i class="os-icon os-icon-hamburger-menu-1"></i>
                            <div class="os-dropdown">
                                <div class="icon-w">
                                    <i class="os-icon os-icon-ui-46"></i>
                                </div>
                                <ul>
                                    <input type="hidden" id="filter-pipeline" value="">
                                    <li onclick="$('#filter-pipeline').val('').change()"><a href="javascript:void(0)"><span>Todos</span></a></li>
                                    <li onclick="$('#filter-pipeline').val(0).change()"><a href="javascript:void(0)"><span>Em Aberto</span></a></li>
                                    <li onclick="$('#filter-pipeline').val(1).change()"><a href="javascript:void(0)" ><span>Resolvendo</span></a></li>
                                    <li onclick="$('#filter-pipeline').val(2).change()"><a href="javascript:void(0)" ><span>Finalizado</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="pipeline-body"></div>
                </div>
            </div>

            <div class="col-md-5">
                <input type="hidden" id="data-issue-id">
                <div class="element-wrapper">
                    <div class="element-box">
                        <h6 class="element-header">Chat</h6>
                        <div class="chat-component">
                            <div class="full-chat-w">
                                <div class="full-chat-i">
                                    <div class="full-chat-middle">
                                        <div class="chat-content-w ps ps--theme_default" data-ps-id="140a96d0-0412-dbe4-8cba-5f97959acd5a">
                                            <div class="chat-content p0">
                                                <div class="icon-click">
                                                    <i class="os-icon os-icon-grid-squares-22"></i>
                                                </div>
                                                <div class="text-click">
                                                    Clique em um caso no menu ao lado para exibir os detalhes
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat-controls">
                                            <div class="chat-input">
                                                <input placeholder="Digite sua mensagem aqui..." type="text">
                                            </div>
                                            <div class="chat-input-extra">
                                                <div class="chat-btn"><a class="btn btn-primary btn-sm" href="#">Enviar</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 issue-data">
                <div class="element-wrapper">
                    <div class="element-box">
                        <div class="icon-click">
                            <i class="os-icon os-icon-grid-squares-22"></i>
                        </div>
                        <div class="text-click">
                            Clique em um caso no menu ao lado para exibir os detalhes
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
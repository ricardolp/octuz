<input type="hidden" id="type" value="change">
<div aria-labelledby="exampleModalLabel" class="modal fade" id="modalContrataPlano" role="dialog" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Alterar Assinatura</h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
            </div>
            <form action="/painel/planos/alterar" method="POST">
                <input type="hidden"  name="plan_code" id="plan_code">
                <div class="modal-body">
                    <div class="pricing-plan col-sm-12">
                        <div class="plan-head">
                            <div class="plan-image">
                                <img alt="" src="/images/plan2.png"/>
                            </div>
                            <div class="plan-name">Mensal</div>
                        </div>
                        <div class="plan-price-w">
                            <div class="price-value">R$ 15,90</div>
                            <div class="price-label">Por Mês</div>
                        </div>
                    </div>
                    <div class="value w-100  credit-card">
                        <div class="fancy-selector-w mt-25">
                            <div class="fancy-selector-current">
                                <div class="fs-img">
                                    <img alt="" src="/images/card1.png"/>
                                </div>
                                <div class="fs-main-info hidden-xs hidden-sm">
                                    <div class="fs-name">Ricardo Pinheiro</div>
                                </div>
                                <div class="fs-extra-info">
                                    1111

                                    <span>Final</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 p-0">
                        <img src="{{ asset('images/moip30px.png') }}" class="pull-left" alt="powered by moip">
                        <button class="btn btn-primary pull-right" type="submit"> Contratar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
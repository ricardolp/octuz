<input type="hidden" id="type" value="create">

<div aria-labelledby="exampleModalLabel" class="modal fade bd-example-modal-lg show" id="modalContrataPlano" role="dialog" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Alterar Assinatura</h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
            </div>
            <form action="/painel/planos/contratar" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <input type="hidden"  name="plan_code" id="plan_code">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group col-sm-6 no-padding pull-left" style="float:left">
                                        <label for="">Validade</label>
                                        <input class="form-control" id="expiration_month" name="expiration_month" placeholder="Mês ex: 12" autocomplete="false" data-mask="99" maxlength="2" type="text">
                                    </div>
                                    <div class="form-group col-sm-6 no-padding pull-left" style="float:left">
                                        <label for="">&nbsp;</label>
                                        <input class="form-control" id="expiration_year" name="expiration_year" placeholder="Ano ex: 20" autocomplete="false" data-mask="99" maxlength="2" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="pricing-plan col-sm-12">
                                <div class="plan-head" style="background-color: transparent;">
                                    <div class="plan-name">Mensal</div>
                                </div>
                                <div class="plan-price-w">
                                    <div class="price-value">R$ 15,90</div>
                                    <div class="price-label">Por Mês</div>
                                </div>
                                <small class="text-left">
                                    *Ao assinar o plano você onfirma estar de acordo com os <a href="">Termos e condições de assinatura</a> da Octuz
                                    <br>
                                    *O valor acima será cobrado por mês no cartão de crédito cadastrado
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 p-0">
                        <img src="{{ asset('images/moip30px.png') }}" class="pull-left" alt="powered by moip">
                        <button class="btn btn-primary pull-right" type="submit"> Contratar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
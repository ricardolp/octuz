@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="">Nome</label>
            <input class="form-control" name="name" placeholder="Digite o nome completo" type="text">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input class="form-control" placeholder="Digite seu e-mail" name="email" type="email">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-email-2-at2"></div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Senha</label>
                    <input class="form-control" placeholder="Password" name="password" type="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <div class="pre-icon os-icon os-icon-fingerprint"></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Confirmar Senha</label>
                    <input class="form-control" placeholder="Password" name="password_confirmation" type="password">
                </div>
            </div>
        </div>
        <div class="buttons-w"><button class="btn btn-block btn-primary">Cadastrar</button></div>
        <br>
        <p class="text-center">Já possui uma conta? <a href="/login" class="card-link">Login</a></p>
    </form>

@endsection

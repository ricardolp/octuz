@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('login') }}" id="form-login">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="">Username</label>
            <input class="form-control"  name="email" placeholder="Digite seu e-mail" type="text" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input class="form-control" name="password" placeholder="Digite sua senha" type="password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
        </div>
        <div class="buttons-w">
            <button class="btn btn-primary btn-block">Entrar</button>
            <button class="btn btn-primary btn-social btn-facebook btn-block" id="fb-login" type="button">Entrar com Faceook</button>
        </div>
        <br>
        <p class="text-center">Não possui uma conta? <a href="/register" class="card-link">Cadastre-se</a></p>
    </form>
    <script src="/js/painel/jquery/dist/jquery.min.js"></script>
    <script>
        $("body").bind("ajaxSend", function(elm, xhr, s){
            token = document.head.querySelector('meta[name="csrf-token"]');
            if (s.type == "POST") {
                xhr.setRequestHeader('X-CSRF-Token', token.content);
            }
        });
    </script>
    <script src="{{ asset('/js/facebook.js') }}" type="text/javascript"></script>
@endsection

<!DOCTYPE html>
<html lang="pt-BR"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>A plataforma que resolve seus problemas | Octuz</title>
    <meta name="description" content="A Octuz é uma plataforma confiável que administra e resolve problemas com empresas. Seja um atendimento mal resolvido com uma operadora, uma cobrança que não deveria estar na sua fatura, a Octuz conecta você com a sua tranquilidade por meio de um atendimento personalizado, rápido e seguro.">
    <meta name="keywords" content="vivo, tim, octuz, procon, reclamação sac, anatel, net, claro, lei do consumidor, resolver problema, procon online. ">
    <meta http-equiv="content-language" content="pt-br">
    <meta name="author" content="Octuz">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('landing/plugins/owlcarousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('landing/css/animation.css') }}">
    <link rel="stylesheet" href="{{ asset('css.scss') }}" />
    <link rel="stylesheet" href="{{ asset('landing/mobirise-icons/mobirise-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('landing/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('landing/css/octuz.css') }}">

    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
</head>
<body>

<div class="page-wrapper">

    <div class="preloader">
        <div class="loader">
            <img src="{{ asset('landing/img/preloader.gif') }}" itemprop="image" alt="">
        </div>
    </div>

    <div class="page-header">
        <header class="header">
            <div class="container">
                <div class="logo">
                    <a  itemprop="url" href="#"><img src="{{ asset('images/logo.png') }}" itemprop="image" alt="Octuz Logo" /></a>
                </div>
                <div class="mobile-menu">
                    <a  itemprop="url" href="#"><i class="ion-android-menu"></i></a>
                </div>
                <div class="main-nav">
                    <ul class="main-nav-inner">
                        <li><a itemprop="url" href="#intro">Início</a></li>
                        <li><a itemprop="url" href="#features">Como Funciona</a></li>
                        <li><a itemprop="url" href="#testimonial">Depoimentos</a></li>
                        <li><a itemprop="url" href="#contact">Contato</a></li>

                       @if(Auth::guest())
                            <li>
                                <a  itemprop="url" href="/login" onclick="window.location.href = '{!! route('login') !!}'">Entrar</a>
                            </li>
                        @else
                            <li class="dropdown dropdown-user nav-item">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                    <span class="user-name">{{ Auth::user()->name }}</span>
                                    <span class="avatar avatar-online">
                                    <img style="width:40px;border-radius:125px;" src="{{ Auth::user()->path_url ?: URL::to('images/user.png') }}" alt="user octuz avatar">
                                    <i></i>
                                </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/painel" onclick="window.location.href = '{!! route('painel.index') !!}'" class="dropdown-item" style="color:#484848;line-height: 40px;">
                                        <i class="ft-user"></i>
                                        Meus Casos
                                    </a>
                                    <a href="/painel/perfil"  onclick="window.location.href = '{!! route('painel.minha-conta') !!}'" class="dropdown-item" style="color:#484848;line-height: 40px;">
                                        <i class="ft-user"></i>
                                        Editar Perfil
                                    </a>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </header>

        <section id="intro" class="intro parallax" data-parallax="scroll" itemprop="image" data-image-src="{{ asset('images/bg.png') }}" alt="banner octuz">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="intro-content wow fadeInUp text-center" data-wow-delay="0.2s">
                            <p class="intro-description" data-wow-delay="0.2s">Não deixe a música de espera ser a trilha sonora da sua vida.<br> Nós resolvemos seu problema enquanto <span>você relaxa</span>.</p>
                            <div class="col-md-6 col-md-offset-3" data-wow-delay="0.4s">
                                <div class="dl-btn-wrapper">
                                    <div class="mc-form top">
                                        <input class="mc-empresa typehead" id="mc-empresa" autocomplete="off" placeholder="Com qual empresa você esta tendo problemas?" required="required" name="empresa" type="text">
                                        <input class="submit-btn btn-resolve hidden-xs" value="Resolver" type="button">
                                        <label for="mc-email"></label>
                                    </div>
                                </div>
                                <input class="submit-btn btn-resolve btn-block hidden-md hidden-lg hidden-sm" value="Resolver" type="button">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section id="features" class="section-padding">
        <div class="single-features-top">
            <div class="container">
                <div class="row lines-bg" id="lines-bg">
                    <div class="col-md-4 text-center col-converse">
                        <div class="col-md-12">
                            <img src="{{ asset('images/converse.svg') }}" class="svg-inject icon-problema" alt="signal" />
                        </div>
                        <div class="col-md-12">
                            <p class="single-description">Conte o que está acontecendo e como você quer que seu problema seja resolvido <b>conversando</b> com a gente.</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center col-relaxe">
                        <div class="col-md-12">
                            <img src="{{ asset('images/relaxe.svg') }}" class="svg-inject icon-problema" alt="signal" />
                        </div>
                        <div class="col-md-12">
                            <p class="single-description">enquanto a Octuz entra em contato com a operadora e <b>resolve</b> seu problema, você aproveita seu tempo!</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center col-solucao">
                        <div class="col-md-12">
                            <img src="{{ asset('images/solucao.svg') }}" class="svg-inject icon-problema" alt="signal" />
                        </div>
                        <div class="col-md-12">
                            <p class="single-description">As informações do seu caso estão seguras em nossa plataforma, e você pode acompanhar tudo do início ao fim.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section id="testimonial" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 center-col">
                    <div class="testimonial-slider wow fadeInDown" data-wow-delay="0.2s">

                        <div class="slider-item">
                            <blockquote>
                                <div class="author-img">
                                    <img src="{{ asset('landing/img/1.jpg') }}" itemprop="image" />
                                </div>
                                <p itemprop="description">
                                    Conheci a <b>Octuz</b> em meados de setembro através de uma reclamação sem sucesso que estava fazendo com a Claro.
                                    Na ocasião era um parcelamento e uma cobrança indevida de valores.
                                    Após passar os meus dados para a empresa, em menos de 24 horas eles já haviam conseguido a solução do meu problema que já se arrastava por semanas.
                                    Posterior a isso também utilizei os serviços da <b>Octuz</b> com a net (um telefone cortado de forma indevida) e também fui representado pela empresa em várias solicitações que necessitei.
                                    Confio plenamente de olhos fechados.
                                    Não perco mais meu tempo em relação a reclamação com empresas que prestam um péssimo serviço.
                                    Recomendo!!!!
                                </p>
                                <footer>
                                    <cite itemprop="name">Paulo Alfredo Rabelo</cite>
                                    <h6>Cliente Claro</h6>
                                </footer>
                            </blockquote>
                        </div>

                        <div class="slider-item">
                            <blockquote>
                                <div class="author-img">
                                    <img src="{{ asset('landing/img/2.jpg') }}" itemprop="image" alt="depoimento octuz carolina" />
                                </div>
                                <p itemprop="description">
                                    Já tive 3 problemas resolvidos pela <b>Octuz</b> no último mês. A resposta deles às minhas solicitações é imediata e eles vão me mandando um update da resolução do problema. Até agora 100% resolvido! Poupa muito tempo, recomendo!
                                </p>
                                <footer>
                                    <cite itemprop="name">Carolina Ayvazian </cite>
                                    <h6>&nbsp;</h6>
                                </footer>
                            </blockquote>
                        </div>

                        <div class="slider-item">
                            <blockquote>
                                <div class="author-img">
                                    <img src="{{ asset('landing/img/3.jpg') }}" itemprop="image" alt="depoimento octuz lana" />
                                </div>
                                <p itemprop="description">
                                    Excelente opção para quem não quer esquentar a cabeça com o péssimo atendimento call center de empresas de telefonia, empresas aéreas, dentre outras. Recomendo!
                                </p>
                                <footer>
                                    <cite itemprop="name">Lana Pedrosa </cite>
                                    <h6>&nbsp;</h6>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="faq" class="section-padding" >
        <div class="container">
            <h2 class="section-header">Faq</h2>
            <div class="margin-bottom"></div>

            <div class="row">
                <div class="col-md-8 center-col mb-sm-80 mb-xs-80">
                    <div class="accordion wow fadeInUp" data-wow-delay="0.2s">
                        <div class="accordion-title">
                            <a href="#">Por que fazemos?</a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            O tempo dos nossos clientes é precioso demais para passarem horas clicando em números e falando com atendentes que não querem ajudar.
                        </div>
                    </div>

                    <div class="accordion wow fadeInUp" data-wow-delay="0.3s">
                        <div class="accordion-title">
                            <a href="#">Por que posso confiar na Octuz?</a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            Nosso intuito é única e exclusivamente ajudar nas suas solicitações. Seus dados são sigilosos e nossa equipe é 100% interna e treinada.
                        </div>
                    </div>

                    <div class="accordion wow fadeInUp" data-wow-delay="0.5s">
                        <div class="accordion-title">
                            <a href="#">E se a Octuz não conseguir resolver meu problema?</a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            Faremos de tudo para resolver seu problema. Caso não seja possível, indicaremos a melhor forma de prosseguir com sua situação.
                        </div>
                    </div>

                    <div class="accordion wow fadeInUp" data-wow-delay="0.6s">
                        <div class="accordion-title">
                            <a href="#">Quanto custa?</a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            Em breve teremos algumas opções de pagamento, mas por enquanto você só precisa pagar R$ 9,90 após seu problema ser resolvido!
                        </div>
                    </div>

                    <div class="accordion wow fadeInUp" data-wow-delay="0.6s">
                        <div class="accordion-title">
                            <a href="#">Por que devo passar meu CPF?</a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            Esse documento é exigido por todas as empresas para confirmação de dados. Sem ele, infelizmente, somos impedidos de agir em seu nome .
                        </div>
                    </div>

                    <div class="accordion wow fadeInUp" data-wow-delay="0.6s">
                        <div class="accordion-title">
                            <a href="#">E se eu não passar meus dados? </a>
                        </div>
                        <div class="accordion-content" itemprop="description">
                            Dificilmente teremos autonomia para resolver ou solicitar algo sem confirmar que estamos te representando (confirmação feita pelo seus dados). Uma alternativa pode ser por meio de uma chamada coletiva entre você e a empresa.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="some-features" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="mockup-text wow fadeInUp" data-wow-delay="0.2s">
                        <h6 class="font-alt">POR QUE CONFIAR NA OCTUZ? </h6>
                        <h2 class="mb-0">Você nunca mais vai precisar ligar para um call center.</h2>
                        <p itemprop="description">
                            Controle todos os seus casos diretamente em nossa plataforma. Não importa a empresa ou o problema, a <b>Octuz</b> é o único canal de atendimento que você precisa para nunca mais perder seu tempo ouvindo música de espera.
                        </p>
                        <div class="store-buttons">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="mockup" data-wow-delay="0.2s">
                        <img src="{{ asset('images/people.png') }}" class="mockup-front wow fadeInUp" data-wow-delay="0.4s"  alt="octuz mockup painel" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="contact" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact text-center">
                        <form id="contact-form" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" name="name" class="form-control contact-form" id="first-name" placeholder="Nome" required="required">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" name="email" class="form-control contact-form" id="email" placeholder="E-mail" required="required">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="email" name="email" class="form-control contact-form" id="email" placeholder="Empresa" required="required">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea rows="6" name="message" class="form-control contact-form" id="description" placeholder="Digite sua mensagem" required="required"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <div class="actions">
                                        <input type="button" value="Enviar" name="submit" id="submitButton" class="btn btn-lg btn-contact-bg" title="Envie sua mensagem!" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer id="footer_section">

        <div class="footer_up">
            <div class="container">
                <div class="row">
                    <div class="footer_responsive">

                        <div class="col-md-3 col-sm-12 footer_contact responsive-footer"> <!-- START ABOUT -->
                            <div class="creativefox-about">
                                <h5 class="footer_header">
                                    <img itemprop="logo" src="{{ asset('images/logo_color.png') }}" class="logo-ft" alt="logo octuz colorida">
                                </h5>
                                <p class="about_description" itemprop="description">Não deixe a música de espera ser a trilha sonora da sua vida.<br> Nós resolvemos seu problema enquanto <span>vocė relaxa</span>.</p>
                            </div>
                        </div> <!-- END ABOUT -->
                        <div class="col-md-2 col-sm-12 footer_contact responsive-footer">
                            <div class="footer_top_menu">
                                <h5 class="footer_header">Saiba Mais</h5>
                                <ul>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="#testimonial">Depoimentos</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="#faq">O que fazemos</a></li>
                                    <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="#intro">Resolver problemas</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-12 footer_contact responsive-footer">
                            <div class="help_links">
                                <h5 class="footer_header">Recursos</h5>
                                <ul class="links_names">
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Termos</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 footer_contact responsive-footer">
                            <div class="help_links">
                                <h5 class="footer_header">Contato</h5>
                                <ul class="links_names">
                                    <li><a href="#" class="normal-text">atendimento@octuz.com</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-12 footer_contact responsive-footer">
                            <div class="help_links">
                                <h5 class="footer_header octuz-text">&copy;Octuz.com</h5>
                                <ul class="links_midias">
                                    <li>
                                        <a target="_blank" href="https://www.facebook.com/OctuzResolve/"><i class="ti-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://twitter.com/octuzresolve"><i class="ti-twitter-alt"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://www.linkedin.com/company/10932410/"><i class="ti-linkedin"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>  <!-- END FOOTER UP -->

        <div class="footer_down footer_content"> <!-- START FOOTER DOWN -->
            <div class='container' >

                <div class='row' >

                    <div class='col-sm-12' >

                        <p class='wow fadeInDown' data-wow-delay='.5s' >
                            &copy; 2017 <a href="http://octuz.com.br">Octuz</a>. All Rights Reserved.
                        </p>

                    </div>

                </div>

            </div>
        </div>
    </footer>

    <div class="modal fade" id="modalTelefonica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="padding: 5px">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <button type="button" class="close close-popup ripe" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </div>
                    <form action="/entrar" method="POST" id="form-telefonica">
                        <input type="hidden" name="csrf" id="csrf">
                        <input type="hidden" name="employer_id" id="employer_id">
                        <input type="hidden" name="account_id" id="account_id">
                        <div class="row hide" data-step="1" data-wow-delay='.5s'>
                            <div class="well fadeIn" >
                                <div class="row">
                                    <div class="col-md-12 text-center" style="padding: 25px;">
                                        <h4>Qual é o problema que deseja solucionar? </h4>
                                        <p>Por favor, conte-nos o que está acontecendo</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 err-pb hidden" style="margin-top: 10px">
                                        <div class="alert alert-danger fade in alert-dismissable">
                                            <strong>Atenção!</strong> Selecione uma opção
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group" style="width: 100%;">
                                            <div id="radioBtn" class="btn-group" style="width: 100%;">
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Funcionamento">Funcionamento</a></div>
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Cobrança">Cobrança</a>
                                                </div>
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Outros">Outros</a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="hint_problema" id="hint_problema" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-12 err-desc-pb hidden" style="margin-top: 10px">
                                        <div class="alert alert-danger fade in alert-dismissable">
                                            <strong>Atenção!</strong> Adicone uma desrição
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top:25px;">
                                        <div class="form-group">
                                            <textarea name="description" placeholder="Conte-nos o que está acontecendo" autocomplete="off" id="problema_descricao" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="recipes" style="margin-top:55px;">
                                        <input id="upload-input" type="file" name="uploads[]" multiple="multiple">
                                        <a href="#" class="upload-btn" style="font-size:18px;color:#32a6c9;"><i class="ti-clip"></i> Anexar arquivo</a>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" style="padding:15px;" class="btn btn-info btn-block js-btn-step" data-orientation="next">Avançar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" data-step="2" data-title="This is the second and last step!">
                            <div class="well fadeIn" data-wow-delay='.5s'>
                                <div class="row">
                                    <div class="col-md-12 text-center" style="padding: 25px;">
                                        <h4>Qual é a solução que você espera?</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 err-sl hidden" style="margin-top: 10px">
                                        <div class="alert alert-danger fade in alert-dismissable">
                                            <strong>Atenção!</strong> Selecione uma opção
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group" style="width: 100%;">
                                            <div id="radioBtn" class="btn-group" style="width: 100%;">
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Reembolso">Reembolso</a></div>
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Cancelamento">Cancelamento</a>
                                                </div>
                                                <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                    <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Outros">Outros</a>
                                                </div>
                                            </div>
                                            <input type="hidden" autocomplete="off" name="hint_solucao" id="hint_solucao">
                                        </div>
                                    </div>
                                    <div class="col-md-12 err-desc-sl hidden" style="margin-top: 10px">
                                        <div class="alert alert-danger fade in alert-dismissable">
                                            <strong>Atenção!</strong> Adicone uma desrição
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top:25px;">
                                        <div class="form-group">
                                            <textarea autocomplete="off" placeholder="Que solução você espera?" name="solution_description" id="solucao_descricao" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" style="padding:15px;" class="btn btn-info btn-block js-btn-step" data-orientation="next">Avançar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row hide" data-step="3" data-title="">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12 text-center" style="padding: 25px;">
                                        <h4>Entrar</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1 error-login"></div>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="email">E-mail</label>
                                            <input type="email" autocomplete="off" class="form-control i-octuz" id="email_login" aria-describedby="emailHelp" placeholder="Digite o e-mail">
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="password">Senha</label>
                                            <input type="password" autocomplete="off" class="form-control i-octuz" id="password_login" aria-describedby="emailHelp" placeholder="Digite a senha">
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-md-offset-1">
                                        <button type="button" style="padding:15px;" class="btn btn-block btn-info btn-login-og">Entrar</button>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="divider horizontal">OU</div>
                                    </div>
                                    <div class="col-md-10 col-md-offset-1 text-center">
                                        <button type="button" style="padding:15px;" class="btn btn-info btn-block" onclick="$('.js-btn-step')[0].click()">Cadastrar</button>
                                        <button type="button" style="padding:15px;" id="fb-login" class="btn btn-block btn-info ">Entrar com Facebook</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row hide" data-step="4" data-title="This is the second and last step!">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12 text-center" style="padding: 25px;">
                                        <h4>Dados do titular</h4>
                                        <p>Seus dados estão seguros, e serão utilizados apenas para conﬁrmações com a empresa em questão.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="error-register">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">Nome completo</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="name" placeholder="Nome do titular">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">E-mail</label>
                                                <input autocomplete="off" type="email" class="form-control i-octuz" id="email_register" placeholder="Endereço de E-mail">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Senha</label>
                                                <input autocomplete="off" type="password" class="form-control i-octuz" id="password" placeholder="Digite uma senha">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="password_confirmation">Confirmar Senha</label>
                                                <input autocomplete="off" type="password" class="form-control i-octuz" id="password_confirmation" placeholder="Confirme a senha">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="address">Endereço completo</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="address" placeholder="Endereço completo">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fone">Telefone com problema</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="fone" placeholder="Telefone com problema">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_fone">Telefone para contato</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="contact_fone"  placeholder="Telefone para contato">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="dt_nascimento">Data de Nascimento</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="dt_nascimento"  placeholder="Data de Nascimento">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="cpf">CPF</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="cpf"  placeholder="CPF do titular">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" style="padding:15px;" class="btn btn-success btn-block btn-register-og">Avançar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" data-step="5" data-title="This is the second and last step!">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12 text-center" style="padding: 25px;">
                                        <h4>Dados do titular</h4>
                                        <p>Seus dados estão seguros, e serão utilizados apenas para conﬁrmações com a empresa em questão.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="error-updater">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">Nome completo</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="name_upd"  aria-describedby="emailHelp" placeholder="Nome do titular">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="address">Endereço completo</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="address_upd"  aria-describedby="emailHelp" placeholder="Endereço completo">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fone">Telefone com problema</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="fone_upd"  aria-describedby="emailHelp" placeholder="Telefone com problema">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_fone">Telefone para contato</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="contact_fone_upd"  aria-describedby="emailHelp" placeholder="Telefone para contato">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="dt_nascimento">Data de Nascimento</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="dt_nascimento_upd"  aria-describedby="emailHelp" placeholder="Data de Nascimento">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="cpf">CPF</label>
                                                <input autocomplete="off" type="text" class="form-control i-octuz" id="cpf_upd"  aria-describedby="emailHelp" placeholder="CPF do titular">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" style="padding:15px;" class="btn btn-success btn-block btn-update-data">Avançar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    (function(h,e,a,t,m,p) {
        m=e.createElement(a);m.async=!0;m.src=t;
        p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
    })(window,document,'script','https://u.heatmap.it/log.js');
</script>

<script type="text/javascript">
    window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'b252a61dec3e0b28906cf8ab56309467a9fd615b');
</script>

<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1698839770440223');
    fbq('track', 'PageView');
    fbq('track', 'Lead');
    fbq('track', 'CompleteRegistration');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1698839770440223&ev=PageView&noscript=1"
    /></noscript>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('landing/js/typehead.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('landing/plugins/jquery.singlePageNav.js') }}"></script>
<script src="{{ asset('landing/plugins/parallax.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxchimp/1.3.0/jquery.ajaxchimp.min.js"></script>
<script src="{{ asset('landing/plugins/particles.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('landing/plugins/owlcarousel/owl.carousel.js') }}"></script>
<script src="{{ asset('landing/js/newsletter.js') }}"></script>
<script src="{{ asset('landing/js/script.js') }}"></script>
<script src="{{ asset('landing/js/jquery-bootstrap-modal-steps.js') }}"></script>
<script src="{{ asset('landing/js/facebook.js') }}"></script>
<script src="{{ asset('landing/js/jquery.mask.min.js') }}"></script>

<script src="https://octuz.com/landing/js/octuz.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-81507317-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
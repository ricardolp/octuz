@extends('admin.layout')

@section('content')
    <input type="hidden" id="issue-id" value="{{ $issue->id }}">
<div class="content-box">
    <div class="col-sm-4 issue-data pull-left">
        <div class="user-profile compact">
            <div class="up-head-w" style="background: url('{!! $issue->empresa->path_cover !!}') 0 30%;">
                <div class="up-main-info">
                    <img src="{!! $issue->empresa->path_logo !!}" class="rounded-circle img-empresa" alt="logo employer">
                </div>
                <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                        <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                    </g>
                </svg>
            </div>
            <div class="up-controls">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="value-pair">
                            <div class="label">Status:</div>
                            <div class="value badge badge-pill badge-{!! $issue->color_status !!}">{!!  $issue->issue_status_description !!}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="up-contents">
                <div class="col-md-12 no-padding">
                    <h4 class="form-section fz-1">
                        <i class="os-icon os-icon-calendar-time mr-10"></i>
                        Iniciada em:
                        <span class="pull-right">{{ $issue->created_at }}</span>
                    </h4>
                </div>
                <div class="col-md-12 no-padding">
                    <h4 class="form-section fz-1">
                        <i class="os-icon os-icon-grid-18 mr-10"></i>
                        Última atualização:<span class="pull-right">{{ $issue->updated_at }}</span>
                    </h4></div>
                <div class="col-md-12 no-padding">
                    <h4 class="form-section fz-1">
                        <i class="os-icon os-icon-others-43 mr-10"></i>
                        Descrição do problema
                    </h4>
                    <p class="justify fz-1">{{ $issue->description }}</p>
                </div>
                <div class="col-md-12 no-padding">
                    <h4 class="form-section fz-1">
                        <i class="os-icon os-icon-others-43 mr-10"></i>
                        Solução Esperada
                    </h4>
                    <p class="justify fz-1">{{ $issue->solution_description }}</p>
                </div>
                <div class="col-md-12 no-padding">
                    <h4 class="form-section fz-1">
                        <i class="os-icon os-icon-documents-03 mr-10"></i>
                        Arquivos:
                    </h4>
                    <div class="ci-content">
                        <div class="ci-file-list">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5 pull-left">
        <input id="data-issue-id" value="33" type="hidden">
        <div class="element-wrapper">
            <div class="element-box">
                <h6 class="element-header">Chat</h6>
                <div class="chat-component">
                    <div class="full-chat-w">
                        <div class="full-chat-i">
                            <div class="full-chat-middle">
                                <div class="chat-content-w ps ps--theme_default ps--active-y" data-ps-id="d0da2a99-12a3-eb55-3b61-b3ae312d4cc6">
                                    <div class="chat-content p0"></div>
                                    <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: -150px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 150px; height: 450px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 113px; height: 337px;"></div></div></div>
                                <div class="chat-controls">
                                    <div class="chat-input">
                                        <input placeholder="Digite sua mensagem aqui..." type="text">
                                    </div>
                                    <div class="chat-input-extra">
                                        <div class="chat-btn"><a class="btn btn-primary btn-sm" href="#">Enviar</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 pull-left p-0">
        <div class="element-box p-0">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h6 class="element-header p-15">
                        {{ $issue->cliente->name }}
                    </h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table style="width: 100%; border-bottom: 1px solid #ccc;">
                        <tbody>

                        <tr class="w-100">
                            <td class="col-md-3 p-15">Nome</td>
                            <td style="padding: 15px;text-align: right;" class="col-md-9">{{ $issue->cliente->name }}</td>
                        </tr>
                        <tr class="w-100">
                            <td class="col-md-3 p-15">Email</td>
                            <td style="padding: 15px;text-align: right;" class="col-md-9">{{ $issue->cliente->email }}</td>
                        </tr>
                        <tr class="w-100">
                            <td class="col-md-3 p-15">CPF</td>
                            <td style="padding: 15px;text-align: right;" class="col-md-9" >{{ $issue->cliente->cpf ?: '-'}}</td>
                        </tr>


                        <tr class="w-100">
                            <td class="col-md-7 p-15">DT Nascimento</td>
                            <td style="padding: 15px;text-align: right;" class="col-md-6">{{ $issue->cliente->dt_nascimento ?: '-' }}</td>
                        </tr>

                        <tr class="w-100">
                            <td class="col-md-3 p-15">Telefone</td>
                            <td style="padding: 15px;text-align: right;" class="col-md-9">{{ $issue->cliente->fone ?: '-' }}</td>
                        </tr>

                        <tr class="w-100">
                            <td class="col-md-7 p-15">Linha com Problema</td>
                            <td class="col-md-5 p-15"></td>
                        </tr>

                        <tr class="w-100">
                            <td class="col-md-3 p-15">Valor</td>
                            <td class="col-md-5 text-right"><div class="balance-value danger">R$ {{ $issue->value }}</div></td>
                        </tr>

                        </tbody>
                    </table>
                    <table style="width: 100%; border-bottom: 1px solid #ccc;">
                        <tbody>
                            <tr style="width: 100%;">
                                <td class="col-md-3 p-15" style="padding: 15px;">Status</td>
                                <td style="padding: 15px;text-align: right;" class="col-md-9">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn- dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" data-id="{{ $issue->id }}" data-status="0" onclick="setStatus(this)" href="javascript:void(0)">Aguardando</a>
                                            <a class="dropdown-item" data-id="{{ $issue->id }}" data-status="1" onclick="setStatus(this)" href="javascript:void(0)">Resolvendo</a>
                                            <a class="dropdown-item" data-id="{{ $issue->id }}" data-status="2" onclick="setStatus(this)" href="javascript:void(0)">Finalizado</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="floated-chat-i">
                        <div class="chat-messages" id="notes" style="padding:15px;max-height: 300px;overflow: auto !important;">
                        </div>
                        <div class="chat-controls p-15">
                            <fieldset>
                                <div class="input-group">
                                    <input class="form-control" id="message-aux"  placeholder="Digite sua mensagem..." type="text">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary btn-note" type="button">Enviar!</button>
                                    </span>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        getNotes()
    </script>
@endsection
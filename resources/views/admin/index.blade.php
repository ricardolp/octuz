@extends('admin.layout')

@section('content')
    <div class="content-box">
        <div class="pipelines-w">
            <div class="row">
                <div class="col-lg-3 col-xxl-3">
                    <div class="pipeline white lined-primary" style="max-height: 600px;overflow: auto;">
                        <div class="pipeline-header">
                            <h5 class="pipeline-name">Aguardando</h5>
                            <div class="pipeline-header-numbers">
                                <div class="pipeline-count"></div>
                            </div>
                        </div>
                        <div class="pipeline-body aguardando" data-status="0"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-xxl-3">
                    <div class="pipeline white lined-success">
                        <div class="pipeline-header">
                            <h5 class="pipeline-name">Resolvendo</h5>
                            <div class="pipeline-header-numbers">
                                <div class="pipeline-count"></div>
                            </div>
                        </div>
                        <div class="pipeline-body resolvendo" data-status="1"></div>
                    </div>
                </div>
                <div class="col-lg-3 col-xxl-3">
                    <div class="pipeline white lined-warning">
                        <div class="pipeline-header">
                            <h5 class="pipeline-name">Finalizado</h5>
                            <div class="pipeline-header-numbers">
                                <div class="pipeline-count"></div>
                            </div>
                        </div>
                        <div class="pipeline-body finalizado" data-status="2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        carregaAguardando()
        carregaResolvendo()
        carregaFinalizado()
    </script>
@endsection
<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


class Employer extends Model {

    use Searchable;

    protected $fillable = [];

}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientCard extends Model
{

    protected $fillable = [
        'brand',
        'end_number',
        'moip_card_id',
        'is_default',
        'user_id'
    ];

}

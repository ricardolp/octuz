<?php

namespace App\Entities;

use App\Jobs\Mail\UpdateIssue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class Issue extends Model
{

    protected $appends = ['issue_status_description', 'color_status'];


    protected $fillable = [
        'description',
        'solution_description',
        'issue_status',
        'hint_problema',
        'hint_solucao',
        'category_id',
        'client_id',
        'employer_id',
        'account_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::updated(function($model){
            dispatch(new UpdateIssue($model));
        });
    }

    public function getIssueStatusDescriptionAttribute()
    {
        switch($this->issue_status){
            case 0:
                return 'Aguardando';
            case 1:
                return 'Em Resolução';
            case 2:
                return 'Resolvido';
            case 3:
                return 'Arquivado';
        }
    }

    public function getColorStatusAttribute()
    {
        switch($this->issue_status){
            case 0:
                return 'default';
            case 1:
                return 'blue';
            case 2:
                return 'green';
            case 3:
                return 'yellow';
        }
    }

    public function cliente()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function empresa()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function assign()
    {
        return $this->hasMany(AssignIssueUser::class, 'issue_id');
    }

    public function arquivos()
    {
        return $this->hasMany(IssueFile::class, 'issue_id');
    }

    public function message()
    {
        return $this->hasMany(Message::class, 'issue_id');
    }

    public function note()
    {
        return $this->hasMany(IssueNote::class, 'issue_id');
    }

    public function getCreatedAtAttribute($value)
    {
        $data = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $data->format('d/m/Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        $data = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $data->format('d/m/Y H:i:s');
    }
}

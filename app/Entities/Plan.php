<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $appends = ['numeric_value'];

    public function getNumericValueAttribute()
    {
        $str = str_replace('.', '', $this->value);
        return $str*10;
    }
}

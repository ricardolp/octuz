<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{


    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'message',
        'status',
        'user_id',
        'issue_id',
    ];

    public function getCreatedAtAttribute($value)
    {
        $data = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $data->format('d/m/Y H:i');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

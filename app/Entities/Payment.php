<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'order_id',
        'status_order',
        'issue_id',
        'payment_id',
        'amount'
    ];
}

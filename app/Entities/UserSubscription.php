<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class UserSubscription extends Model
{

    protected $dates = ['expired_at'];

    protected $fillable = [
        'status',
        'status_payment',
        'code',
        'subscription_moip_id',
        'moip_account',
        'user_id',
        'last_card',
        'holder_name',
        'brand',
        'expired_at',
        'payment_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

<?php

namespace App\Entities;

use App\Entities\ClientCard;
use App\Entities\Issue;
use App\Entities\Plan;
use App\Entities\UserAccount;
use App\Jobs\Mail\WelcomeMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $appends = ['method_payment_description'];

    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'zipcode',
        'state',
        'city',
        'complement',
        'district',
        'number_residence',
        'cpf',
        'dt_nascimento',
        'hash',
        'path_url',
        'facebook_id',
        'fone',
        'role',
        'method_payment',
        'moip_id',
        'plan_id',
        'subscriber_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function($model){
            dispatch(new WelcomeMail($model));
        });
    }

    public function card()
    {
        return $this->hasMany(ClientCard::class, 'user_id');
    }

    public function account()
    {
        return $this->hasMany(UserAccount::class, 'client_id');
    }

    public function plano()
    {
        return $this->hasOne(Plan::class, 'id', 'plan_id');
    }

    public function getPathUrlAttribute($value)
    {
        return $value ?: URL::to('images/user.png');
    }

    public function subscription()
    {
        return $this->hasMany(UserSubscription::class, 'user_id');
    }

    public function getMethodPaymentDescriptionAttribute()
    {
        switch ($this->method_payment){
            case 'credito':
                return 'Crédito';
            case 'boleto':
                return 'Boleto';
        }
    }




}

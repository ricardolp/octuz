<?php

namespace App\Entities;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AssignIssueUser extends Model
{

    protected $fillable = [
        'user_id',
        'issue_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



}

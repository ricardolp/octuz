<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class IssueFile extends Model
{

    protected $fillable = [
        'path_url',
        'issue_id'
    ];

}

<?php

namespace App\Jobs\Painel\Subscription;

use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class UpdateSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {

        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user->id);
        $content = 'Sua assinatura foi atualizada com sucesso, seu cartão de crédito
         utilizado na sua assinatura foi atualizado com sucesso e já será utilizado na próxima cobrança de seu plano.';
        Mail::send('emails.template', ['title' => 'Assinatura Atualizada', 'name' => $user->name, 'content' => $content], function ($message) use($user) {
            $message->from('atendimento@octuz.com', 'Atendimento Octuz');
            $message->subject('Alteração de Assinatura');
            $message->to($user->email);
        });
    }
}

<?php

namespace App\Jobs\Painel\Subscription;

use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class CancelSubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $subscription;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->subscription->user_id);
        $content = 'Sua assinatura foi cancelada com sucesso, lamentamos se algo nao ocorreu como o planejado mas você pode ficar a vontade para voltar quando quiser';
        Mail::send('emails.template', ['title' => 'Assinatura cancelada', 'name' => $user->name, 'content' => $content], function ($message) use($user) {
            $message->from('atendimento@octuz.com', 'Atendimento Octuz');
            $message->subject('Alteração de Assinatura');
            $message->to($user->email);
        });
    }
}

<?php

namespace App\Jobs\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class UpdateIssue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $issue;

    /**
     * Create a new job instance.
     *
     * @param $issue
     */
    public function __construct($issue)
    {

        $this->issue = $issue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->issue->cliente;
        Mail::send('emails.issue.update', ['title' => 'Seu Caso foi atualizado'], function ($message) use($user) {
            $message->from('atendimento@octuz.com', 'Atendimento Octuz');
            $message->subject('Octuz - Caso Atualizado');
            $message->to($user->email);
        });
    }
}

<?php

namespace App\Http\Controllers\Painel;


use App\Entities\Issue;
use Illuminate\Support\Facades\Auth;

class IndexController
{

    public function index()
    {
        $casos = Issue::where('client_id', Auth::user()->id)->count();
        $recuperados = Issue::where('client_id', Auth::user()->id)->where('issue_status', 3)->sum('value');
        $total = Issue::where('client_id', Auth::user()->id)->whereIn('issue_status', [0,1])->count();

        return view('painel.index')->with(compact('casos', 'recuperados', 'total'));
    }

}
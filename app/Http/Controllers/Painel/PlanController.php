<?php

namespace App\Http\Controllers\Painel;


use App\Entities\Plan;
use App\Entities\User;
use App\Entities\UserSubscription;
use App\Http\Controllers\Base;
use App\Http\Controllers\Payment\CustomerController;
use App\Http\Controllers\Payment\SubscriptionController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Payment\PlanController as PlanData;

class PlanController
{

    use Base;

    public function index()
    {
        $user = Auth::user();
        $subscription = $user->subscription->whereIn('status', ['ACTIVE', 'SUSPENDED', 'OVERDUE'])->all();


        if(count($subscription) > 0){
            $key = array_keys($subscription);
            $subscription = ['holder' => $subscription[$key[0]]->holder_name, 'card' => $subscription[$key[0]]->last_card, 'brand' => $subscription[$key[0]]->brand] ?: null;
        } else {
            $subscription = null;
        }

        return view('painel.planos.index')->with(compact('subscription'));
    }

    public function show($plano)
    {
        $plano = Plan::where('name', ucfirst($plano))->get();
        $plano = $plano->first() ?: null;

        $subscription = Auth::user()->subscription->whereIn('status', ['ACTIVE', 'OVERDUE'])->last();

        if(is_null($plano)) return abort(404);

        return view('painel.planos.contrata')->with(compact('plano', 'subscription'));
    }

    public function update(Request $req)
    {
        $data = $req->all();

        if($data['plan_code'] === 'plano03'){
            $this->cancel();
        } else {
            $this->change($data);
        }

        return redirect()->route('painel.minha-conta')->with('success', 2);
    }

    public function cancel()
    {
        $user = Auth::user();
        $plans = UserSubscription::whereIn('status', ['ACTIVE', 'SUSPENDED', 'OVERDUE'])->where('user_id', $user->id)->get();
        $plans = $plans->last();
        $plans->update(['status' => 'CANCELED']);
        $plans->save();
        $user->plan_id = 3;
        $user->save();
    }

    public function store(Request $req)
    {
        $data = $req->all();
        $user = Auth::user();
        $check = $user->subscription->whereIn('status', ['ACTIVE', 'SUSPENDED', 'OVERDUE'])->count();
        $resp = true;

        if($user->plan_id === 3 || is_null($user->plan_id)) {
            if($check > 0) {
                return redirect()->route('painel.minha-conta')->with('error', 2);
            } else {
                $resp = $this->create($data);
            }
        } else {
            $this->change($data);
        }

        if($resp === true){
            return redirect()->route('painel.minha-conta')->with('success', 2);
        } else {
            return back()->with('error', 'Algo deu errado, vefifique os dados e tente novamente');
        }


    }

    public function change($data)
    {
        $user = Auth::user();
        $subscription = $user->subscription->last();
        $json = PlanData::basicInfo($data);
        $plano = Plan::where('plan_code', $data['plan_code'])->first();

        $user->plan_id = $plano->id;
        $user->save();
        $change = $this->send($json, 'assinaturas/v1/subscriptions/'.$subscription->code, 'PUT');

        return true;

    }

    public function create($data)
    {
        $user = Auth::user();

        $customer = new CustomerController($data, $user);
        $customer = $customer->get();
        $json = PlanData::basicInfo($data);
        $customer = array_merge($json, $customer);
        $subscription = $this->send($customer, 'assinaturas/v1/subscriptions?new_customer=true');

        $err = (array) $subscription;
        if(isset($err['errors'])){
            if(!count($err['errors']) == 0){
                return $err;
            }
        }

        UserSubscription::create([
            'status' => $subscription->status,
            'code' => $subscription->code,
            'subscription_moip_id' => $subscription->id,
            'moip_account' => $subscription->moip_account,
            'last_card' => $subscription->customer->billing_info->credit_card->last_four_digits,
            'holder_name' => $subscription->customer->billing_info->credit_card->holder_name,
            'brand' => $subscription->customer->billing_info->credit_card->brand,
            'expired_at' => now()->addMonth(1),
            'payment_at' => now(),
            'user_id' => Auth::user()->id
        ]);
        $plano = Plan::where('plan_code', $data['plan_code'])->first();

        $user = User::find($user->id);
        $user->update(['subscriber_id' => $subscription->customer->code, 'plan_id' => $plano->id]);
        $user->save();

        return true;
    }



}
<?php

namespace App\Http\Controllers\Painel;

use App\Entities\Issue;
use App\Entities\User;
use App\Entities\UserSubscription;
use App\Http\Controllers\Payment\ClienteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController
{

    public function index()
    {
        $casos = Issue::where('client_id', Auth::user()->id)->count();
        $recuperados = Issue::where('client_id', Auth::user()->id)->where('issue_status', 3)->sum('value');
        $resolvidos = Issue::where('client_id', Auth::user()->id)->whereIn('issue_status', [3])->count();

        $overdue = UserSubscription::where('status', 'OVERDUE')->count();
        $overdue = $overdue > 0 ? true : false;

        $subscription = Auth::user()->subscription->whereIn('status', ['ACTIVE', 'OVERDUE'])->first() ?: null;

        if(is_null(Auth::user()->moip_id)){
            $create = new ClienteController();
            $create->create([
                'id' => Auth::user()->id,
                'name' => Auth::user()->name,
                'email' => 'ricardoluiz_p@hotmail.com',
                'dt_nascimento' => '1990-11-01',
                'cpf' => '07065416933'
            ]);
        }

        return view('painel.user.index')->with(compact('casos', 'recuperados', 'resolvidos', 'metodo', 'overdue', 'subscription'));
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $user = User::find(Auth::user()->id);
        $user->update($data);
        $user->save();
        return back()->with('success', 1);
    }

}
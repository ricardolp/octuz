<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Base;
use App\Http\Controllers\Payment\PlanController;
use App\Jobs\Painel\Subscription\CancelSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentMethodController extends Controller
{
    use Base;

    public function edit()
    {
        return view('painel.planos.pagamento');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $plano = new PlanController();
        return $plano->changePayment($data);
    }

    public function destroy()
    {
        return view('painel.planos.cancelar');
    }

    public function delete(Request $request)
    {
        $data = $request->all();
        $subscription = Auth::user()->subscription->whereIn('status', ['ACTIVE', 'OVERDUE'])->first() ?: null;
        $resp = $this->send([], 'assinaturas/v1/subscriptions/'.$subscription->code.'/cancel', 'PUT');
        dispatch(new CancelSubscription($subscription));
        return redirect('/painel/minha-conta')->with('success', 3);
    }
}

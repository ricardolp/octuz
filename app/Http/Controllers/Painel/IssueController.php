<?php

namespace App\Http\Controllers\Painel;


use App\Entities\Employer;
use App\Entities\Issue;
use App\Entities\IssueFile;
use App\Http\Controllers\Payment\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class IssueController
{

    public function index()
    {
        return view('painel.issue.index');
    }

    public function create()
    {
        return view('painel.issue.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['client_id'] = Auth::user()->id;
        $data['category_id'] = Employer::find($data['employer_id'])->category_id;

        $issue = Issue::create($data);

        if(isset($data['files'])){
            foreach ($data['files'] as $file) {
                IssueFile::create(['issue_id' => $issue->id, 'path_url' => URL::to("uploads/$file")]);
            }
        }

        $payment = [];

        $order = new OrderController();
        $order = $order->createOrder(['id' => $issue->id, 'user_id' => Auth::user()->id], 1990);

        $payment['order_id'] = $order->getId();



        return redirect()->back()->with('success', 'Caso adicionado com sucesso');
    }

}
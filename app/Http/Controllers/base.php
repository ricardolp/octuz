<?php
namespace App\Http\Controllers;

use Carbon\Carbon;

trait Base {
    public function getDateExpirate($expirate)
    {
        $date = explode('/', $expirate);
        return ['year' => isset($date[1]) ? $date[1] : 00, 'month' => isset($date[0]) ? $date[0] : 00];
    }

    function clean($string) {
        $string = str_replace('-', '', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function formatFone($fone)
    {
        $fone = str_replace(' ', '', $fone);
        $fone = str_replace('-', '', $fone);
        $fone = explode(')', $fone);
        return ['area' => str_replace('(', '', $fone[0]), 'number' => $fone[1]];
    }

    public function formatBirth($data)
    {
        $data = Carbon::createFromFormat('d/m/Y', $data);
        return ['day' => $data->format('d'), 'month' => $data->format('m'), 'year' => $data->format('Y')];
    }

    public function send($json, $url, $method=null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('MOIP_URL_SANDBOX') . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json));

        if($method === 'POST' || is_null($method)){
            curl_setopt($ch, CURLOPT_POST, 1);
        } elseif ($method === 'PUT') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }


        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Authorization: Basic ".base64_encode(env('MOIP_TOKEN').':'.env('MOIP_KEY'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close ($ch);

        $return = json_decode($result);
        return $return;
    }
}
<?php

namespace App\Http\Controllers\Api;


use App\Entities\Issue;
use Illuminate\Http\Request;

class UserIssueController
{

    public function index($user, Request $request)
    {

        if($request->has('status') && (!empty($request->get('status')) || $request->get('status') === '0')){
            $where = ' AND issue_status="'.$request->get('status').'"';
        } else {
            $where = '';
        }

        $data = Issue::where('client_id', $user)
            ->whereRaw("id != 0 $where")
            ->with('empresa')
            ->orderBy('id', 'desc')
            ->get();

        return ['success' => true, 'data' => $data];
    }

}
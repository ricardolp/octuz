<?php

namespace App\Http\Controllers\Api;


use App\Entities\Issue;
use App\Entities\User;
use App\Http\Controllers\Payment\OrderController;
use App\Jobs\SendOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IssueController
{

    public function index(Request $request)
    {
        if($request->has('status') && (!empty($request->get('status')) || $request->get('status') === '0')){
            $where = ' AND issue_status="'.$request->get('status').'"';
        } else {
            $where = '';
        }

        $data = Issue::whereRaw("id != 0 $where")
            ->with('empresa')
            ->orderBy('id', 'desc')
            ->get();

        return ['succes' => true, 'data' => $data];
    }

    public function getAddress($fone)
    {
        $fone = str_replace(' ', '', $fone);
        $fone = str_replace('-', '', $fone);
        $fone = explode(')', $fone);

        return ['ddd' => str_replace('(', '', $fone[0]), 'fone' => $fone[1]];
    }

    public function getBirthDate(Request $request)
    {
        $data = $request->all();

        $data = Carbon::createFromFormat('d/m/Y', $data['data']);

        return ['dd' => $data->format('d'), 'mm' => $data->format('m'), 'yy' => $data->format('Y')];
    }

    public function update($iss, Request $request)
    {

        $data = $request->all();
        $iss = Issue::find($iss);
        $iss->update(['issue_status' => $data['issue_status']]);
        $iss->save();

        if($data['issue_status'] == 2){
            $this->pay($iss);
        }

        return ['success' => true];
    }

    public function pay($issue)
    {
        $user = User::find($issue->client_id);

        if(is_null($user->plan_id) || $user->plan_id == 0) {
            $order = new OrderController();
            $order->createOrder([
                'user_id' => $issue->client_id,
                'id' => $issue->id,
                'value' => $issue->value
            ]);

            //dispatch(new SendOrder($order->getId()));
        }
    }

    public function show($id)
    {
        $data = Issue::with('empresa')
            ->with('cliente')
            ->with('arquivos')
            ->where('id', $id)
            ->get();

        return ['succes' => true, 'data' => $data->first()];
    }
}
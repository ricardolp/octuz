<?php

namespace App\Http\Controllers\Api;

use App\Entities\User;
use App\Entities\UserAccount;
use App\Jobs\Mail\WelcomeMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $data = $request->all();


        $user = User::where('email', $data['email'])->get();

        if($user->count() > 0){
            $user = $user->first();

            if(Hash::check($data['password'], $user->password)){

                $user = Auth::user();

                if($user->hash === '' || $user->hash === null){
                    $user->hash = Uuid::uuid4();
                    $user->save();
                }

                return ['success' => true, 'data' => ['encrypted_hash' => $user->hash]];
            }

            return ['success' => false, 'message' => 'Senha incorreta'];
        }

        return ['success' => false, 'message' => 'Usuário não encontrado'];

    }

    public function update(Request $request)
    {
        $data = $request->all();
        $user = User::where('hash', $data['csrf'])->get();

        if($user->count() > 0){
            $user = $user->first();

            $this->validate($request, [
                'name_upd' => 'required',
                'address_upd' => 'required',
                'fone_upd' => 'required',
                'contact_fone_upd' => 'required',
                'dt_nascimento_upd' => 'required',
                'cpf_upd' => 'required'
            ]);

            $user->update([
                'name' => $data['name_upd'],
                'address' => $data['address_upd'],
                'fone' => $data['fone_upd'],
                'dt_nascimento' => $data['dt_nascimento_upd'],
                'cpf' => $data['cpf_upd']
            ]);

            $user->save();
        }

        return ['success' => true];
    }

    public function register(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
            'address' => 'required',
            'fone' => 'required',
            'contact_fone' => 'required',
            'dt_nascimento' => 'required',
            'cpf' => 'required'
        ]);

        $user = User::where('email', $data['email'])->get();

        if($user->count() > 0){
            return ['success' => false, 'message' => 'Usuário ja cadastrado'];
        }

        if($data['password_confirmation'] != $data['password']){
            return ['success' => false, 'message' => 'As senhas não conferem'];
        }

        unset($data['password_confirmation']);

        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $user->hash = Uuid::uuid4();
        $user->save();

        $account = UserAccount::create([
            'numero_conta' => $data['fone'],
            'client_id' => $user->id,
            'employer_id' => $data['employer_id']
        ]);

        return ['success' => true, 'data' => ['encrypted_hash' => $user->hash, 'account' => $account->id]];
    }

}

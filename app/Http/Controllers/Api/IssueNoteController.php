<?php

namespace App\Http\Controllers\Api;

use App\Entities\IssueNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IssueNoteController extends Controller
{

    public function index($issue, Request $request)
    {
        $user = $request->has('user') ? '(case user_id WHEN '.$request->get('user').' THEN true ELSE false END) as self' : ' false as self';

        $notes = IssueNote::where('issue_id', $issue)
            ->select('*', DB::raw($user))
            ->with('user')
            ->get();

        return ['success' => 'data', 'data' => $notes];

    }

    public function store(Request $request)
    {
        $data = $request->all();

        $iss = IssueNote::create($data);

        $notes = IssueNote::where('id', $iss->id)
            ->select('*', DB::raw('true as self'))
            ->with('user')
            ->first();

        return ['success' => true, 'data' => $notes];
    }

}

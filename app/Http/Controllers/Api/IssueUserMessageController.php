<?php

namespace App\Http\Controllers\Api;

use App\Entities\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IssueUserMessageController extends Controller
{

    public function index($issue, $user)
    {

        $messages = Message::where('issue_id', $issue)
            ->select('*', DB::raw("(case user_id WHEN $user THEN true ELSE false END) as self"),
                DB::raw("date_format(created_at, '%H:%i') as time"),
                DB::raw("date_format(created_at, '%d-%m-%Y') as date"))
            ->orderBy('created_at')
            ->with('user')
            ->get();

        return ['success' => true, 'data' => $messages];

    }

    public function store(Request $req, $issue, $user)
    {
        $data = $req->all();

        $data['user_id'] = $user;
        $data['issue_id'] = $issue;
        $data['status'] = 0;

        $message = Message::create($data);
        $message = Message::where('id', $message->id)
            ->select('*', DB::raw("(case user_id WHEN $user THEN true ELSE false END) as self"),
                DB::raw("date_format(created_at, '%H:%i') as time"),
                DB::raw("date_format(created_at, '%d-%m-%Y') as date"))
            ->with('user')
            ->first();
        return ['success' => true, 'data' => $message];

    }

}

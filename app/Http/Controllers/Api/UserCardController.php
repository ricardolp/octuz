<?php

namespace App\Http\Controllers\Api;

use App\Entities\ClientCard;
use App\Http\Controllers\Payment\CardController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCardController extends Controller
{

    public function store($user, Request $request)
    {
        $data = $request->all();

        $card = new CardController();
        $new_card = $card->create($data, $user);

        $card = ClientCard::create([
            'user_id' => $user,
            'moip_card_id' => $new_card->getId(),
            'brand' =>  $new_card->getBrand(),
            'end_number' => $new_card->getLast4()
        ]);
    }

}

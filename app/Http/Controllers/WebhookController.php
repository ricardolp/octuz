<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Entities\UserSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{

    public function receive(Request $request)
    {
        $data = $request->all();
        Log::info(json_encode($request->all()));
        switch ($data['event']) {
            case 'customer.updated':
                $this->updateCustomer($data['resource']['billing_info']['credit_cards'][0]);
                break;
            case 'subscription.canceled':
                $this->cancelSubscription($data);
                break;
        }
    }

    public function updateCustomer($data)
    {
        $subscription = UserSubscription::whereIn('status', ['ACTIVE', 'OVERDUE'])->first();
        $subscription->update([
            'last_card' => $data['last_four_digits'],
            'holder_name' => $data['holder_name'],
            'brand' => $data['brand'],
        ]);
        $subscription->save();
    }

    public function cancelSubscription($data)
    {
        $subscription = UserSubscription::whereIn('status', ['ACTIVE', 'OVERDUE'])->where('code', $data['resource']['code'] )->first();
        $subscription->update([
            'status' => 'CANCELED',
        ]);
        $subscription->save();

        $user = User::find($subscription->user_id);
        $user->plan_id = 3;

    }

}

<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class SocialController extends Controller
{

    public function facebook(Request $request)
    {
        Auth::logout();
        $data = $request->all()['dados'];
        $user = User::where('facebook_id', $data['id'])->get();
        $cc = false;

        if($user->count() <= 0){
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'path_url' => 'https://graph.facebook.com/'.$data['id'].'/picture?type=large',
                'facebook_id' => $data['id']
            ]);
            $cc = true;
        } else {
            $user = $user->first();
            $cc = false;
        }

        $user->hash = Uuid::uuid4();
        $user->save();

        return ['success' => true, 'data' => ['encrypted_hash' => $user->hash, 'account' => $cc]];
    }
}

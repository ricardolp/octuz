<?php

namespace App\Http\Controllers\Payment;

use App\Entities\Plan;
use App\Entities\UserSubscription;
use App\Http\Controllers\Base;
use App\Jobs\Painel\Subscription\UpdateSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
    use Base;

    public static function basicInfo($data)
    {
        $data = Plan::where('plan_code', $data['plan_code'])->get();
        $subs = UserSubscription::all()->last();
        $subs = is_null($subs) ? 1 : $subs->id+1;

        return [
            "code" => "SIG-" . uniqid() ,
            "amount" => $data->first()->numeric_value,
            "plan" => [
                "code"=> $data->first()->plan_code
            ],
            "payment_method" => "CREDIT_CARD",
        ];
    }


    public function changePayment($data)
    {
        $user = Auth::user();
        $url = "assinaturas/v1/customers/".$user->subscriber_id."/billing_infos";
        $exp = $this->getDateExpirate($data['validade']);
        dispatch(new UpdateSubscription($user));
        $arr = [
            "credit_card" => [
                "holder_name" => $data['holder_name'],
                "number" => str_replace(' ', '', $data['credit_card']),
                "expiration_month" => $exp['month'],
                "expiration_year" => $exp['year']
            ]
        ];

        $resp = (array) $this->send($arr, $url, 'PUT');

        if(isset($resp['errors'])){
            $err = $resp['errors'][0]->description;
            return redirect('/painel/assinatura/pagamento')->with('error', $err);
        }

        return redirect('/painel/minha-conta')->with('success', 2);
    }


}

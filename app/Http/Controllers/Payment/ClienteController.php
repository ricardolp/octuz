<?php

namespace App\Http\Controllers\Payment;

use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Moip\Moip;
use Moip\Auth\BasicAuth;

class ClienteController extends Controller
{

    /**
     * @var Moip
     */
    private $moip;

    public function __construct()
    {
        $token =  env('MOIP_TOKEN');
        $key =  env('MOIP_KEY');
        $this->moip = new Moip(new BasicAuth($token, $key), Moip::ENDPOINT_SANDBOX);
    }

    public function create(array $user)
    {
        $customer = $this->moip->customers()->setOwnId(uniqid())
            ->setFullname($user['name'])
            ->setEmail($user['email'])
            ->setBirthDate($user['dt_nascimento'])
            ->setTaxDocument($user['cpf'])
            ->create();

        $data = User::find($user['id']);
        $data->update(['moip_id' => $customer->getId()]);
        $data->save();
        return;
    }

}

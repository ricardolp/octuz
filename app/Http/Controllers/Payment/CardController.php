<?php

namespace App\Http\Controllers\Payment;

use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Moip\Auth\BasicAuth;
use Moip\Moip;

class CardController extends Controller
{

    /**
     * @var Moip
     */
    private $moip;

    public function __construct()
    {
        $token =  env('MOIP_TOKEN');
        $key =  env('MOIP_KEY');
        $this->moip = new Moip(new BasicAuth($token, $key), Moip::ENDPOINT_SANDBOX);
    }

    public function create(array $card, $user)
    {
        $user = User::find($user);

        $nascimento = Carbon::createFromFormat('d/m/Y', $card['dt_nascimento']);
        $card['dt_nascimento'] = $nascimento->format('Y-m-d');

        $customer = $this->moip->customers()->get($user->moip_id);

        $customer->addAddress('SHIPPING',
                $card['address'], $card['number_complement'],
                $card['district'], $card['city'], $card['state'],
                $card['zipcode'], $card['number_complement'])
                ->addAddress('BILLING',
                    $card['address'], $card['number_complement'],
                    $card['district'], $card['city'], $card['state'],
                    $card['zipcode'], $card['number_complement']);

        $fone = $this->sanitizeString($user->fone);
        $dd = substr($fone, 0,2);
        $number = substr($fone, 2,strlen($fone));

        $card = $this->moip->customers()->creditCard()
            ->setExpirationMonth($card['expMonth'])
            ->setExpirationYear($card['expYear'])
            ->setNumber($card['number'])
            ->setCVC($card['cvc'])
            ->setFullName($card['nome_titular'])
            ->setBirthDate($card['dt_nascimento'])
            ->setTaxDocument('CPF', $card['cpf_titular'])
            ->setPhone('55',$dd,$number)
            ->create($user->moip_id);

        return $card;

    }

    function sanitizeString($string) {
        $what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );
        $by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','','','','','','','','','','','','','','','','','','','','','','','' );
        return str_replace($what, $by, $string);
    }
}

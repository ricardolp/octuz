<?php

namespace App\Http\Controllers\Payment;

use App\Entities\Payment;
use App\Entities\User;
use Moip\Auth\BasicAuth;
use Moip\Moip;

class OrderController
{

    /**
     * @var Moip
     */
    private $moip;

    public function __construct()
    {
        $token =  env('MOIP_TOKEN');
        $key =  env('MOIP_KEY');
        $this->moip = new Moip(new BasicAuth($token, $key), Moip::ENDPOINT_SANDBOX);
    }

    public function createOrder($data)
    {
        $logo_uri = 'https://cdn.moip.com.br/wp-content/uploads/2016/05/02163352/logo-moip.png';
        $expiration_date = new \DateTime();
        $instruction_lines = ['INSTRUÇÃO 1', 'INSTRUÇÃO 2', 'INSTRUÇÃO 3'];
        $user = User::find($data['user_id']);

        $customer = $this->moip->customers()->get($user->moip_id);
        $value = isset($data['value']) ? $data['value'] : 1000;
        $order = $this->moip->orders()->setOwnId(uniqid())
            ->addItem('Resolução - Caso '.$data['id'],1, "Resolução Octuz", $value)
            ->setShippingAmount(0)->setAddition(0)->setDiscount(0)
            ->setCustomer($customer)
            ->create();

        $payment = $order->payments()
            ->setBoleto($expiration_date, $logo_uri, $instruction_lines)
            ->execute();

        return $order;
    }

}
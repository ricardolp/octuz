<?php

namespace App\Http\Controllers\Payment;

use App\Entities\Plan;
use App\Entities\User;
use App\Entities\UserSubscription;
use App\Http\Controllers\Base;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    use Base;
    /**
     * @var User
     */
    private $user;
    private $data;

    public function __construct(array $data, User $user)
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function get()
    {
        $json = [];
        $json['customer'] = $this->customer($this->data);
        $json['customer']['address'] = $this->address();
        $json['customer']['billing_info'] = $this->payment($this->data);
        return $json;
    }

    public function customer($data)
    {

        $fone = $this->formatFone($data['fone']);
        $birth = $this->formatBirth($data['dt_nascimento']);

        return [
            "code" => 'CUS-'.strtoupper(uniqid()),
            "email" => $this->user->email,
            "fullname" => $data['holder_name'],
            "cpf" => $this->clean($data['holder_cpf']),
            "phone_number" => $fone['number'],
            "phone_area_code" => $fone['area'],
            "birthdate_day" => $birth['day'],
            "birthdate_month" => $birth['month'],
            "birthdate_year" => $birth['year'],
        ];
    }

    public function address()
    {
        return [
            "street" =>  Auth::user()->address,
            "number" =>  Auth::user()->number_residence,
            "complement" => Auth::user()->complement ?: 'casa',
            "district" =>  Auth::user()->district,
            "city" => Auth::user()->city,
            "state" =>  Auth::user()->state,
            "country" => 'BRA',
            "zipcode" => str_replace('-', '', Auth::user()->zipcode),
        ];
    }

    public function payment($data)
    {
        $exp = $this->getDateExpirate($data['validade']);
        return [
            "credit_card" => [
                "holder_name" => $data['holder_name'],
                "number" => str_replace(' ', '', $data['credit_card']),
                "expiration_month" => $exp['month'],
                "expiration_year" => $exp['year']
            ]
        ];
    }



}

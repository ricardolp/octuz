<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Moip\Auth\BasicAuth;
use Moip\Moip;

class SubscriptionController extends Controller
{

    /**
     * @var Moip
     */
    private $moip;

    public function __construct()
    {
        $token =  env('MOIP_TOKEN');
        $key =  env('MOIP_KEY');
        $this->moip = new Moip(new BasicAuth($token, $key), Moip::ENDPOINT_SANDBOX);
    }




}

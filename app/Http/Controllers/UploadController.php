<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class UploadController extends Controller
{

    public function upload(Request $request)
    {
        $data = $request->all();

        $arr = [];
        foreach ($data['uploads'] as $upload) {
            $file = $upload;
            $name = Uuid::uuid4();
            $file->move(public_path('uploads') , $name . '.' .$file->getClientOriginalExtension());
            array_push($arr, $name . '.' .$file->getClientOriginalExtension());
        }

        return ['success' => true, 'data' => $arr];
    }

}

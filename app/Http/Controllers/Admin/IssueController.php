<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Issue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IssueController extends Controller
{

    public function show($id)
    {
        $issue = Issue::where('id', $id)
            ->with('cliente')
            ->with('empresa')
            ->first();

        return view('admin.issue.show')->with(compact('issue'));
    }

}

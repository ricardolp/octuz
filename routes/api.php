<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->post('login', 'Api\\AuthController@login');
Route::middleware('api')->post('register', 'Api\\AuthController@register');
Route::middleware('api')->post('update/profile', 'Api\\AuthController@update');

Route::middleware('api')->get('format/fone/{fone}', 'Api\\IssueController@getAddress');
Route::middleware('api')->post('format/data', 'Api\\IssueController@getBirthDate');

Route::middleware('api')->resource('issue', 'Api\\IssueController');
Route::middleware('api')->resource('user.issue', 'Api\\UserIssueController');
Route::middleware('api')->resource('user.card', 'Api\\UserCardController');
Route::middleware('api')->resource('issue.user.messages', 'Api\\IssueUserMessageController');
Route::middleware('api')->resource('empresa', 'Api\\EmployerController');
Route::middleware('api')->resource('issue.user.messages', 'Api\\IssueUserMessageController');
Route::middleware('api')->resource('issue.note', 'Api\\IssueNoteController');



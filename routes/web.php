<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Auth::loginUsingId(1);

Route::group(['prefix' => 'painel', 'as' => 'painel.', 'middleware' => 'auth', 'namespace' => 'Painel'], function(){
    Route::get('', function(){
        return redirect()->route('painel.index');
    });
    Route::get('/casos', 'IssueController@index')->name('index');
    Route::get('/casos/novo', 'IssueController@create');
    Route::post('/casos', 'IssueController@store');
    Route::get('/minha-conta', 'UserController@index')->name('minha-conta');
    Route::post('/minha-conta', 'UserController@update');
    Route::get('/assinatura', 'PlanController@index');
    Route::get('/assinatura/contratar/{plano}', 'PlanController@show');
    Route::get('/assinatura/pagamento', 'PaymentMethodController@edit');
    Route::get('/assinatura/cancelar', 'PaymentMethodController@destroy');
    Route::post('/planos/contratar', 'PlanController@store');
    Route::post('/planos/alterar', 'PlanController@update');
    Route::post('/assinatura/pagamento', 'PaymentMethodController@update');
    Route::post('/assinatura/cancelar', 'PaymentMethodController@delete');
});

Route::post('/webhook/assinaturas', 'WebhookController@receive');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function(){
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('/casos/{id}', 'IssueController@show');
});

Route::post('facebook/login', 'Auth\\SocialController@facebook');
Route::post('entrar', 'Painel\\AuthController@login');
Route::post('upload', 'UploadController@upload');
Auth::routes();

Route::get('/home', function(){
    return redirect('/painel');
});
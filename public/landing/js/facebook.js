//Load the Facebook JS SDK
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

window.fbAsyncInit = function() {
    FB.init({
        appId      : '374727946279613',
        xfbml      : true,
        version    : 'v2.8',
        status     : true,
        cookie     : true
    });

    // Tipos de permissão que irá ser pedida ao usuário.
    var permissions = [
        'email',
        'public_profile',
    ].join(',');

    // Campos que vão ser retornados após o login ser confirmado
    var fields = [
        'id',
        'name',
        'first_name',
        'last_name',
        'email',
    ].join(',');


    function showDetails() {
        FB.api('/me', {fields: fields}, function(details) {
            $('#userdata').html(JSON.stringify(details, null, '\t'));
            try {
                $.ajax({
                    method: "POST",
                    url: '/facebook/login',
                    data: {dados: details}
                }).done(function(data) {
                    $("#csrf").val(data.data.encrypted_hash);

                    if(data.data.account === true){
                        $('.js-btn-step')[0].click()
                        $('.js-btn-step')[0].click()
                    } else {
                        $("#form-telefonica").submit();
                    }

                    //$(".js-btn-step").click();
                });
            }
            catch (e) {
                alert("Pagina inexistente");
            }

            $('#fb-login').attr('style', 'display:none;');
        });
    }


    $('#fb-login').click(function(){
        fbAsyncInit();
        FB.login(function(response) {
            if(response.authResponse) {
                showDetails();
            }
        }, {scope: permissions});
    });

};
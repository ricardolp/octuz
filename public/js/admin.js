$(document).ready(function(){
    carregaChat()
})

function loadData(status, container)
{
    $.get('/templates/admin/pipeline-item.mst', function(tpl) {
        $.get("/api/issue?status="+status)
            .done(function( data ) {
                $(".pipeline-body."+container).empty();

                if(data.data.length === 0) {
                    $(".pipeline-body."+container).append("<h5 class='text-center'>Nenhum caso a ser exibido</h5>");
                } else {
                    $.each(data.data, function(key,val) {
                        if(status === 2){
                            console.log(".pipeline-body."+container)
                        }
                        $(".pipeline-body."+container).append(Mustache.to_html(tpl, val));
                    });
                }
            });
    })
}

function carregaAguardando(){
   loadData(0, 'aguardando')
}

function carregaResolvendo(){
    loadData(1, 'resolvendo')
}

function carregaFinalizado(){
    loadData(2, 'finalizado')
}

function getNotes()
{
    $.get('/templates/admin/notes.mst', function(tpl) {
        var issue = $("#issue-id").val()
        var user = $("body").data('id');
        $.get("/api/issue/"+issue+"/note?user="+user)
            .done(function( data ) {
                $.each(data.data, function(key,val) {
                    $("#notes").append(Mustache.to_html(tpl, val));
                });
            });
    })
}

function setStatus(el)
{
    var issue = $(el).data('id')
    var status = $(el).data('status')
    $.post('/api/issue/'+issue, { issue_status: status, _method: 'patch' }, function (){ })
        .done(function(data){
            location.reload()
        })
}

$('.btn-note').on('click', function () {
    add_note($('.message-aux'));
    return false;
});

function add_note($input){
    var issue = $("#issue-id").val();
    var user = $("body").data('id');
    $.post('/api/issue/'+issue+'/note', { message: $('#message-aux').val(), issue_id: issue, user_id: user }, function (){ $('#message-aux').val('') })
        .done(function(data){
            $('#notes').append('<div class="chat-message self">' +
                '<div class="chat-message-content-w"><div class="chat-message-content">' + data.data.message + '</div></div>' +
                '<div class="chat-message-avatar"><img alt="" src="'+data.data.user.path_url+'">' +
                '</div>' +
                '</div>');
        })
}

function carregaChat(){
    $.get('/templates/admin/chat.mst', function(tpl) {
        var issue = $("#issue-id").val()
        var user = $("body").data('id');
        $.get("/api/issue/"+issue+"/user/"+user+"/messages")
            .done(function( data ) {
                $(".chat-content").empty()
                $.each(data.data, function(key,val) {
                    $(".chat-content").append(Mustache.to_html(tpl, val));
                });

                var $messages_w = $('.chat-content-w');
                $messages_w.scrollTop($messages_w.prop("scrollHeight"));
                $messages_w.perfectScrollbar('update');
            });
    })
}

$('.chat-btn a').on('click', function () {
    add_full_chat_message($('.chat-input input'));
    return false;
});

$('.chat-input input').on('keypress', function (e) {
    if (e.which == 13) {
        add_full_chat_message($(this));
        return false;
    }
})

function add_full_chat_message($input)
{
    var issue = $("#issue-id").val();
    var user = $("body").data('id');
    $.post('/api/issue/'+issue+'/user/'+user+'/messages', { message: $input.val() }, function (){ $input.val('') })
        .done(function(data){
            $('.chat-content').append('<div class="chat-message self">' +
                '<div class="chat-message-content-w"><div class="chat-message-content">' + data.data.message + '</div></div>' +
                '<div class="chat-message-date">'+data.data.time+'</div><div class="chat-message-avatar"><img alt="" src="'+data.data.user.path_url+'">' +
                '</div>' +
                '</div>');
            var $messages_w = $('.chat-content-w');
            $messages_w.scrollTop($messages_w.prop("scrollHeight"));
            $messages_w.perfectScrollbar('update');
        })

}
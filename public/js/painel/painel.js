$(document).ready(function(){
    carregaCasos()

    $.get("http://desenvolvimento.ws/api/empresa?find=", function(data){
        $(".typehead").typeahead({
            display: 'nome',
            source:data
        });
    },'json');

    $('#modalTelefonica').modalSteps();

    $('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);

        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    })
})

$('.upload-btn').on('click', function (){
    $('#upload-input').click();
    $('.progress-bar').text('0%');
    $('.progress-bar').width('0%');
});

$('#upload-input').on('change', function(){

    var files = $(this).get(0).files;
    if (files.length > 0){
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('uploads[]', file, file.name);
        }

        $.ajax({
            url: '/uploads',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $.each(data.data, function (i, v){
                    $("#recipes").append('<input type="hidden" id="files" name="files[]" value="'+v+'">')
                })
            },
            xhr: function() {
                var xhr = new XMLHttpRequest();

                xhr.upload.addEventListener('progress', function(evt) {

                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.progress-bar').text(percentComplete + '%');
                        $('.progress-bar').width(percentComplete + '%');
                        if (percentComplete === 100) {
                            $('.progress-bar').html('100%');
                        }
                    }

                }, false);

                return xhr;
            }
        });
    }
});


$("#filter-pipeline").on('change', function(){
    $(".pipeline-body").empty();
    $(".pipeline-body").append('<img src="/images/loader.gif" style="max-width: 100%;"/>')
    carregaCasos()
})

function carregaCasos(){
    $.get('/templates/painel/pipeline-item.mst', function(tpl) {
        var filter = $("#filter-pipeline").val()
        var user = $("body").data('id');
        $.get("/api/user/"+user+"/issue?status="+filter)
            .done(function( data ) {
                $(".pipeline-body").empty();

                if(data.data.length === 0) {
                    $(".pipeline-body").append("<h5 class='text-center'>Nenhum caso a ser exibido</h5>");
                } else {
                    $.each(data.data, function(key,val) {
                        $(".pipeline-body").append(Mustache.to_html(tpl, val));
                    });

                    $(".pipeline-item").on('click', function(){
                        carregaDadosCaso($(this).data('id'))
                    })
                }
            });
    })
}

function carregaDadosCaso(id)
{
    $(".chat-content").empty()
    $(".chat-content").append('<img src="/images/loader.gif" style="max-width: 100%;"/>')

    $.get('/templates/painel/issue-data.mst', function(tpl){;
        $.get("/api/issue/"+id)
            .done(function( data ) {
                $(".issue-data").empty();
                $(".issue-data").append(Mustache.to_html(tpl, data.data));
                carregaChat(id)
            });
    })
}

function carregaChat(issue)
{
    $.get('/templates/painel/chat.mst', function(tpl) {
        var user = $("body").data('id');
        $.get("/api/issue/"+issue+"/user/"+user+"/messages")
        .done(function( data ) {
            $("#data-issue-id").val(issue)
            $(".chat-content").empty()
            $.each(data.data, function(key,val) {
                $(".chat-content").append(Mustache.to_html(tpl, val));
            });

            var $messages_w = $('.chat-content-w');
            $messages_w.scrollTop($messages_w.prop("scrollHeight"));
            $messages_w.perfectScrollbar('update');
        });
    })
}

$(".btn-unlock").on('click', function(){
    var inputs = $("#formValidate").find('input')
    var selects = $("#formValidate").find('select')
    $.each(inputs, function (i, v) { if($(v).hasClass('readonly') === false) ($(v).attr('readonly') === false) ? $(v).attr('readonly', true) : $(v).attr('readonly', false) })
    $.each(selects, function (i, v) { if($(v).hasClass('readonly') === false) ($(v).attr('readonly') === false) ? $(v).attr('readonly', true) : $(v).attr('readonly', false) })
    $(".btn-submit-f").attr('disabled', false)
})

$.getJSON('/estados-cidades.json', function (data) {
    var estado = $(".estado");

    var options = '<option value="" disabled selected>Estado</option>';

    $.each(data.estados, function (key, val) {
        options += '<option value="' + val.sigla + '" id="' + val.sigla + '">' + val.sigla + '</option>';
    });

    estado.html(options);

    var estado_selected = $("#estado_selected").val();
    if(estado_selected != ""){
        $('#'+ estado_selected).attr('selected', true);
    }

    estado.change(function(){
        var cidade = $(".cidade");
        var options_cidades = '<option value="0" selected disabled>Cidade</option>';
        var str = "";

        $(".estado option:selected").each(function () {
            str += $(this).text();
        });

        $.each(data.estados, function (key, val) {
            if(val.sigla == str) {
                $.each(val.cidades, function (key_city, val_city) {
                    options_cidades += '<option value="' + val_city + '" id="' + val_city + '">' + val_city + '</option>';
                });
            }
        });
        cidade.html(options_cidades);

        var cidade_selected = $("#cidade_selected").val();
        if(cidade_selected != ""){
            $('#'+ cidade_selected).attr('selected', true);
        }

    }).change();
});

$('.chat-btn a').on('click', function () {
    add_full_chat_message($('.chat-input input'));
    return false;
});

$('.chat-input input').on('keypress', function (e) {
    if (e.which == 13) {
        add_full_chat_message($(this));
        return false;
    }
})

function add_full_chat_message($input)
{
    var issue = $("#data-issue-id").val();
    var user = $("body").data('id');
    $.post('/api/issue/'+issue+'/user/'+user+'/messages', { message: $input.val() }, function (){ $input.val('') })
        .done(function(data){
            $('.chat-content').append('<div class="chat-message self">' +
                '<div class="chat-message-content-w"><div class="chat-message-content">' + data.data.message + '</div></div>' +
                '<div class="chat-message-date">'+data.data.time+'</div><div class="chat-message-avatar"><img alt="" src="'+data.data.user.path_url+'">' +
                '</div>' +
                '</div>');
            var $messages_w = $('.chat-content-w');
            $messages_w.scrollTop($messages_w.prop("scrollHeight"));
            $messages_w.perfectScrollbar('update');
        })

}
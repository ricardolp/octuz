var moip = new MoipAssinaturas("MWBWP6EEJQN2SPW3JIVDTQFPFSFYHT9K");


var build_customer = function() {
    const URL = $("#url").val()
    const fone = $("#phone").val()
    const data = $("#dt_nascimento").val()

    var customer_params = {
        fullname: $("#fullname").val(),
        email: $("#email").val(),
        code: new Date().getTime(),
        cpf : $("#cpf").val(),
        billing_info: build_billing_info(),
        address: build_address()
    }

    customer_params.phone_area_code = ''
    customer_params.phone_area_code = ''
    customer_params.birthdate_day = ''
    customer_params.birthdate_month = ''
    customer_params.birthdate_year = ''

    $.get(URL+'/api/format/fone/'+fone, function(data){
        customer_params.phone_area_code = data.ddd
        customer_params.phone_area_code = data.fone
    })

    $.post(URL+'/api/format/data', { data: data }, function(data){
        customer_params.birthdate_day = data.dd
        customer_params.birthdate_month = data.mm
        customer_params.birthdate_year = data.yy
    })

    return new Customer(customer_params);
};

var build_billing_info = function() {
    var billing_info_params = {
        fullname : $("#holder_name").val(),
        expiration_month: $("#expiration_month").val(),
        expiration_year: $("#expiration_year").val(),
        credit_card_number: $("#credit_card").val()
    };
    return new BillingInfo(billing_info_params);
};

var build_address = function() {

    var address_params = {
        street: $("#rua").val(),
        number: $("#numero").val(),
        complement: $("#complemento").val(),
        district: $("#bairro").val(),
        zipcode: $("#cep").val(),
        city: $("#cidade").val(),
        state: $("#estado").val(),
        country: "BRA"
    };

    return new Address(address_params);
};
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('password')->nullable();
            $table->string('cpf')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('district')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->boolean('free_issue')->nullable()->default(1);
            $table->string('complement')->nullable();
            $table->string('number_residence')->nullable();
            $table->string('dt_nascimento')->nullable();
            $table->string('hash')->nullable();
            $table->string('path_url')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('fone')->nullable();
            $table->string('moip_id')->nullable();
            $table->string('subscriber_id')->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->string('role', 10)->nullable()->default('user');
            $table->rememberToken();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

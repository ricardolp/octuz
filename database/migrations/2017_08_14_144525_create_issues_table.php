<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->text('solution_description')->nullable();
            $table->string('issue_status')->default(0)->nullable();
            $table->string('hint_problema')->nullable();
            $table->string('hint_solucao')->nullable();
            $table->double('value', 8,2)->nullable()->default(0);
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('users');
            $table->integer('employer_id')->unsigned()->nullable();
            $table->foreign('employer_id')->references('id')->on('employers');
            $table->integer('account_id')->unsigned()->nullable();
            $table->foreign('account_id')->references('id')->on('user_accounts');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issues');
	}

}
